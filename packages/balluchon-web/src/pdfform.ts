import { jsPDF } from "jspdf";
import logoData from "./assets/logo/balluchonData";
import questions from './constants/questions';
import "./assets/NotoMono-Regular-normal";
import "./assets/NotoSans-normal";
import "./assets/NotoSans-bold";

interface GeneratePdfParams {
    nbPages: number;
    printNumbers: boolean;
    title: string;
    pageFormat: ('a4-p' | 'a5-p');
}

function generatePdfForm({ nbPages = 8, printNumbers = true, title = "Balade test du 21 janvier -- quartier Jaurès", pageFormat = 'a5-p' }: GeneratePdfParams) {
    // Default export is a4 paper, portrait, using millimeters for units
    const format = pageFormat.split('-')[0];
    const orientation = pageFormat.split('-')[1] === 'p' ? 'p' : 'l';
    const doc = new jsPDF({ format: format, orientation: orientation });
    doc.setFont('NotoSans', 'normal');
    const buildingQuestions = questions.doorOfBuildingTags;
    const addressQuestions = questions.doorAddressTags;
    // - formulaire caractérisation des portes     
    const page_width = 148;
    const bw = 10,
        bh = 10,
        margin_q_o = 4;

    for (var i = 1; i <= nbPages; i++) {
        let ww = bw,
            hh = bh + 24,  //+ (i - 1) * h +
            hh_d = bh + 16,
            ww_d = page_width - 35,
            // ww2 = bw ,
            h0_initial = 5;

        doc.addImage(logoData, "PNG", 6, 7, 18, 18);
        doc.setFontSize(9).text("formulaire de caractérisation des portes", 26, 12);
        doc.setFontSize(9).text("pour la contribution sur OSM avec", 26, 17);
        doc.setFontSize(11).text("Balluchon", 26, 23);
        // 
        doc.setFontSize(10).text('Porte n°', ww_d, hh_d,);
        doc.rect(ww_d + 16, hh_d - 7, 10, 10);
        if (printNumbers) {
            const num_w = doc.getTextWidth(i + "");
            doc.setFontSize(13).text(i + "", ww_d + 20 - num_w / 2, hh_d);
        }
        doc.setFontSize(10).text(title, ww - 3, hh);

        doc.setDrawColor('0.5');
        doc.setLineWidth(0.5).line(6, hh + 3, page_width - 6, hh + 3);
        doc.setFontSize(9);
        doc.setLineWidth(0.2);
        let h0 = h0_initial;

        // tags form
        buildingQuestions.forEach(q => {
            if (h0 > 180) { h0 = h0_initial; ww = ww + 67; }
            h0 += 6;
            doc.setFont('NotoSans', 'bold');
            doc.text(q.question, ww + 0, hh + h0);
            doc.setFont('NotoSans', 'normal');
            h0 += 6;
            if (q.type === 'select' && q.options) {
                q.options.forEach(op => {
                    doc.rect(ww + margin_q_o - 5, hh + h0 - 2.5, 3, 3);
                    doc.text(op.label, ww + margin_q_o, hh + h0);
                    h0 += 6;
                });
            }
            else {
                doc.rect(ww + margin_q_o - 5, hh + h0 - 3.8, 30, 8);
                h0 += 4;
            }
        });

        // addresses form
        h0 += 3;
        doc.setLineWidth(0.5).line(ww - 4, hh + h0 + 4, ww + 38, hh + h0 + 4);
        doc.setLineWidth(0.2);
        h0 += 11;
        doc.setFont('NotoSans', 'bold');
        doc.text("Addresse", ww + 0, hh + h0);
        doc.setFont('NotoSans', 'normal');
        h0 += 2;
        addressQuestions.forEach((q, i) => {
            h0 += 6;
            doc.text(q.question, ww, hh + h0);
            h0 += 4;
            if (q.type === 'number' || q.question.toLowerCase().includes('num')) {
                doc.rect(ww + margin_q_o - 5, hh + h0 - 1.8, 30, 8);
                h0 += 6;
            }
            else {
                doc.rect(ww + margin_q_o - 5, hh + h0 - 1.8, 60, 8);
                h0 += 6;
            }

        });

        // notes
        h0 += 3;
        doc.setLineWidth(0.5).line(ww - 4, hh + h0 + 4, ww + 38, hh + h0 + 4);
        doc.setLineWidth(0.2);
        // h0 += 11;
        // doc.setFont('NotoSans', 'bold');
        // doc.text("Notes", ww + 0, hh + h0);
        // doc.setFont('NotoSans', 'normal');
        // h0 += 4;
        // doc.rect(ww +margin_q_o - 5, hh + h0 - 1.8, 60, 35);

        if (i < nbPages) { doc.addPage(); }
    }
    //doc.text(30,10 , 'balluchon.od4m.limos.fr')
    doc.save("formulaire-portes-balluchon.pdf");
}

export default generatePdfForm;