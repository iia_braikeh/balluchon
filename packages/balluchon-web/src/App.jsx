import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import UserProvider from './contexts/UserProvider';
import Home from './pages/Home';

import { Provider } from 'react-redux';
import store from './store';

const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter basename='/build'>
        <UserProvider>
          <Switch>
            <Route path={['/comeback', '/']} component={Home} />
            <Redirect to='/' />
          </Switch>
        </UserProvider>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
