# 1. Features list

Date: 2020-09-06

## Context

We need to record the architectural decisions made on this project.

## 

- [x] Geo-localisation
- [ ] Nomatim search
- [x] Keyboard navigation
- [x] Link to OSM page
- [ ] Cache for modifications (last 24hours)
- [ ] Notification if zoom too low
- [ ] Notification if you quit NewDoor before validation
