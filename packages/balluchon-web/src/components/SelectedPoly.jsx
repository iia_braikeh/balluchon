import React, { Component } from 'react';
import { Map } from 'osm-ui-react';
import PropTypes from 'prop-types';
import DoorNode from './DoorNode';
import { connect } from 'react-redux';
import {
  newDoorModesAlong,
  newDoorModesNodes,
  alterDoorModesNodes,
} from '../modes';
const possibleDoorTagKeys = [
  'addr:housenumber',
  'addr:street',
  'door',
  'entrance',
];

class SelectedPoly extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sendPoint: false,
      savedPoint: null,
      mouseOnPoint: null,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.mode === 'newDoor' &&
      this.props.marker !== prevProps.marker &&
      this.props.marker === false
    ) {
      console.log('LOOOG');
      this.setState({ savedPoint: null, sendPoint: true });
    }
    if (this.props.mode !== prevProps.mode && this.props.mode === 'newDoor') {
      this.setState({ savedPoint: this.props.currentDoorLatlng });
    }
  }

  handlePoint = (params) => {
    const { coords, index, hashed, nodeId, nodeOfWay } = params;
    console.log(coords, index, nodeId);
    if (nodeId && nodeOfWay) {
      console.log('sur un noeud du chemin');
    }
    if (nodeId && !nodeOfWay) {
      console.log('sur un noeud libre');
    }
    const latlng = [coords.lat, coords.lng];
    this.props.newDoorLatlng(latlng);
    const wayId = this.props.selectedPoly.poly.refByPositionsHash[hashed];
    this.props.newDoorSegment(wayId, index);
    this.setState({
      savedPoint: latlng,
      sendPoint: false,
    });
  };

  mouseOnPoint = (nodeId, nodeProps) => {
    this.setState({ mouseOnPoint: `node/${nodeId}` });
    this.props.selectAlterDoor(
      this.props.selectedPoly.id,
      `node/${nodeId}`,
      nodeProps
    );
  };
  selectAlterDoor = (nodeId, nodeProps) => {
    this.props.setSelectedAlterDoor();
    this.setState({ mouseOnPoint: `node/${nodeId}` });
    this.props.selectAlterDoor(
      this.props.selectedPoly.id,
      `node/${nodeId}`,
      nodeProps
    );
  };

  render() {
    const { mousePos, mapref } = this.props;
    const { sendPoint, savedPoint, mouseOnPoint } = this.state;
    const positions = this.props.selectedPoly.poly
      ? this.props.selectedPoly.poly.positions
      : null;
    const nodesPositions = this.props.selectedPoly.poly
      ? [
          ...this.props.selectedPoly.poly.nodes,
          ...this.props.selectedPoly.poly.moreNodes,
        ]
      : null;
    return (
      <div>
        {this.props.mode !== 'trip' && positions && (
          <>
            {newDoorModesAlong.includes(this.props.mode) && (
              <Map.Along
                map={mapref.leafletElement}
                showPoint={true}
                waysPositions={positions}
                allowInside={this.props.newDoorOptions.allowInside}
                mousePos={mousePos}
                pointColor={this.props.marker === true ? '#001199' : '#990011'}
                forcePosition={
                  this.props.marker === false ? savedPoint : undefined
                }
                magnetic={this.props.newDoorOptions.magneticDistance >= 0}
                magneticDistance={this.props.newDoorOptions.magneticDistance}
                sendPoint={sendPoint}
                handlePoint={this.handlePoint}
              />
            )}

            {newDoorModesNodes.includes(this.props.mode) &&
              nodesPositions.map((node) => {
                // console.log(node.tag);
                const possibleDoor =
                  node.tag &&
                  node.tag.some((t) => possibleDoorTagKeys.includes(t.k));
                return (
                  <DoorNode
                    key={node.id}
                    possibleDoor={possibleDoor}
                    center={[node.lat, node.lon]}
                  />
                );
              })}
            {this.props.mode === 'selectAlterDoor' &&
              nodesPositions.map((node) => {
                // console.log(node);
                let nodeProps = {
                  latlng: [node.lat, node.lon],
                  version: node.version,
                };
                const possibleDoor =
                  node.tag &&
                  node.tag.some((t) => possibleDoorTagKeys.includes(t.k));

                if (possibleDoor) {
                  const reducer = (accumulator, tag) => {
                    return { ...accumulator, [`${tag.k}`]: tag.v };
                  };
                  const tags = node.tag.filter((t) => !t.k.startsWith('addr'));
                  const address = node.tag.filter((t) =>
                    t.k.startsWith('addr')
                  );

                  nodeProps = {
                    ...nodeProps,
                    tags: tags.reduce(reducer, {}),
                    address: address.reduce(reducer, {}),
                  };
                }
                const mouseOver = mouseOnPoint === `node/${node.id}`;
                return (
                  <DoorNode
                    key={node.id}
                    possibleDoor={possibleDoor}
                    selected={mouseOver}
                    center={[node.lat, node.lon]}
                    onClick={() => this.selectAlterDoor(node.id, nodeProps)}
                    onMouseOver={() => this.mouseOnPoint(node.id, nodeProps)}
                  />
                );
              })}
            {alterDoorModesNodes.includes(this.props.mode) &&
              nodesPositions.map((node) => {
                const possibleDoor =
                  node.tag &&
                  node.tag.some((t) => possibleDoorTagKeys.includes(t.k));
                const selected =
                  this.props.selectedAlterDoor === `node/${node.id}`;
                return (
                  <DoorNode
                    key={node.id}
                    possibleDoor={possibleDoor}
                    selected={selected}
                    center={[node.lat, node.lon]}
                  />
                );
              })}
            <Map.Polygon
              positions={positions}
              color={'#009911'}
              fillColor={'#298011'}
              stroke={true}
            />
          </>
        )}
      </div>
    );
  }
}

const mapState = (state, ownprops) => ({
  mode: state.mode,
  currentDoorLatlng: state.newDoor.currentProps.latlng,
  marker: state.newDoorMarker,
  selectedPoly: state.selectedPoly,
  newDoorOptions: state.newDoorOptions,
  selectedAlterDoor: state.newAlterDoor.currentNodeId,
});

const mapDispatch = (dispatch) => ({
  newDoorLatlng: (latlng) => dispatch.newDoor.newDoorLatlng(latlng),
  newDoorSegment: (wayId, index) =>
    dispatch.newDoor.newDoorSegment({ wayId, index }),
  setSelectedAlterDoor: (_) => dispatch.mode.setMode('selectedAlterDoor'),
  selectAlterDoor: (elementId, nodeId, nodeProps) =>
    dispatch.newAlterDoor.alterDoor({ elementId, nodeId, nodeProps }),
});

export default connect(mapState, mapDispatch)(SelectedPoly);
