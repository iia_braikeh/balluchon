import yellowCircle from '../assets/yellowCircle.png';
import redCircle from '../assets/redCircle.png';
import purpleCircle from '../assets/purpleCircle.png';

function Legend(props) {
  return (
    <div class='info legend'>
      <h4>Informations des portes / entrées</h4>
      <img src={yellowCircle} alt='icône ronde jaune' />{' '}
      <span title='Absence des tags porte et entrée'>Non renseignées</span>
      <br />
      <img src={redCircle} alt='icône ronde rouge' />{' '}
      <span title='Présence tag porte OU tag entrée'>Peu renseignées</span>
      <br />
      <img src={purpleCircle} alt='icône ronde violette' />{' '}
      <span title='Présence tag porte ET tag entrée'>Renseignées</span>
    </div>
  );
}

export default Legend;
