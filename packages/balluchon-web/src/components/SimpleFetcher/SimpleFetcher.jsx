// thks https://github.com/PaulLeCam/react-leaflet/issues/332

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import osmtogeojson from 'osmtogeojson';
import L from 'leaflet';
import { Map } from 'osm-ui-react';
import { connect } from 'react-redux';
import {
  yellowCircleIcon,
  redCircleIcon,
  purpleCircleIcon,
} from '../../helpers/markers';
import { pointInPolygon, flattenBounds } from '../../helpers/geometry';
import UserProvider from '../../contexts/UserProvider';
import simpleQueries from './simpleQueries';
import polygonStyle from './polygonsStyle';

// fetch and display data

class SimpleFetcher extends Component {
  static contextType = UserProvider.context;
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dataNodes: [],
    };
    this.geoJsonLayer = React.createRef();
    this.geoJsonNodesLayer = React.createRef();
  }
  onEachFeature = (feature, layer) => {
    if (feature.properties.name) {
      const tooltipContent = feature.properties.name || '';
      const tooltip = () => tooltipContent;
      layer.bindTooltip(tooltip);
    }

    layer.on({
      mouseover: (e) => {
        this.setState({ mouseSel: feature.id });
      },
      mouseout: (e) => {
        this.setState({ mouseSel: null });
      },
      click: (e) => {
        e.originalEvent.view.L.DomEvent.stopPropagation(e);
        console.log('feature: ', feature);
        if (
          (this.props.mode === 'trip' || this.props.mode === 'selectedPoly') &&
          this.state.loadSelect !== true
        ) {
          if (!this.context) {
            this.props.addNotification('notConnected');
          } else {
            this.props.handleSelectStart();

            fetch(`/api/get/${feature.id}`)
              .then((response) => response.json())
              .then(
                (data) => {
                  let bounds;
                  // if (feature.id.startsWith('way')) {
                  bounds = L.latLngBounds(data.positions);
                  // }
                  // else if (feature.id.startsWith('rel')) {
                  //     bounds = L.latLngBounds(data.positions[0])
                  // }
                  bounds = [
                    [bounds._southWest.lat, bounds._southWest.lng],
                    [bounds._northEast.lat, bounds._northEast.lng],
                  ];
                  console.log(bounds);

                  this.props.selectPolygon(data, bounds);
                },
                (err) => {
                  this.setState({ err });
                  console.log(err);
                  this.handleFetchEnd(err);
                }
              );
            this.props.handleSelectEnd();
          }
        }
        this.props.propagate(e);
      },
    });
  };

  onEachNodeFeature = (feature, layer) => {
    layer.on({
      click: (e) => {
        console.log('feature: ', feature);
      },
    });
  };

  pointToLayer = (feature, latlng) => {
    if (feature.geometry.type === 'Point') {
      // console.log('feature: ', feature);
      if (
        !('door' in feature.properties) &&
        !('entrance' in feature.properties)
      ) {
        return new L.Marker(latlng, { icon: yellowCircleIcon }).setOpacity(
          this.props.nodeOpacity
        );
      } else if (
        !('door' in feature.properties) ||
        !('entrance' in feature.properties)
      ) {
        return new L.Marker(latlng, { icon: redCircleIcon }).setOpacity(
          this.props.nodeOpacity
        );
      }
      return new L.Marker(latlng, { icon: purpleCircleIcon }).setOpacity(
        this.props.nodeOpacity
      );
      // return null;
    }
  };

  style = (feature) => {
    if (
      this.props.mode === 'newDoor' ||
      this.props.mode === 'changeset' ||
      this.props.mode === 'selectAlterDoor'
    ) {
      return false;
    }
    if (this.state.mouseSel === feature.id) {
      return polygonStyle.mouseOverStyle;
    }
    // added door on this way or relation
    if (this.props.newDoorChangedList.includes(feature.id)) {
      return polygonStyle.modifiedDoorStyle;
    }
    if (this.props.visitedPolys.includes(feature.id)) {
      return polygonStyle.visitedStyle;
    }
    // default case
    return polygonStyle.defaultStyle;
  };

  handleFetchEnd = (err) => {
    this.props.handleFetchEnd(err);
  };

  componentDidUpdate(prevProps, prevState) {
    const geoNodes = this.geoJsonNodesLayer.current.leafletElement;
    geoNodes.eachLayer((layer) => {
      layer.setOpacity(this.props.nodeOpacity);
    });

    const { params, loadFetch, loadSelect } = this.props;

    if (loadFetch === false && prevProps.loadFetch !== loadFetch) {
      this.setState({ loadFetch: false });
    }
    if (loadSelect === false && prevProps.loadSelect !== loadSelect) {
      this.setState({ loadSelect: false });
    }
    if (loadFetch === true && prevProps.loadFetch !== loadFetch) {
      this.setState({ loadFetch: true });
    }
    if (loadSelect === true && prevProps.loadSelect !== loadSelect) {
      this.setState({ loadSelect: true });
    }
    if (params === null || this.state.loadFetch === true) {
      console.log('out');
      return;
    }
    console.log('loading buildings now ...');

    // preparing query
    const aroundValue = 50;
    let req = '';
    const position = [params.center.lat, params.center.lng].join();
    const bounds = flattenBounds(params.bounds);
    switch (params.context) {
      case 'building':
        req = simpleQueries.buildingAndPossibleDoorsQuery({ bounds });
        break;
      case 'building2':
        req = simpleQueries.building2Query({ aroundValue, position });
        break;
      default:
    }

    fetch(
      // `https://z.overpass-api.de/api/interpreter?data=${req}`
      `https://overpass-api.de/api/interpreter?data=${req}`
      // "https://bikewise.org:443/api/v2/locations/markers?proximity_square=10"
    )
      .then((response) => response.json())
      .then((osm) => osmtogeojson(osm))
      .then(
        (data) => {
          let gotSomething = [false, false];
          // let foundFeature = false;
          if (data.features && data.features.length > 0) {
            let newData = data.features;
            const oldData = prevState.data;
            const oldDataNodes = prevState.dataNodes;
            // const inLists = (oldLists, feature) =>
            //     oldLists.some((oldList) => { return inList(oldList, feature); });
            const inList = (oldList, feature) =>
              oldList.some((oldFeature) => {
                return oldFeature.id === feature.id;
              });
            // newData = newData.filter(feature => { return !inLists(oldData, feature); });

            let newNodes = newData.filter((feature) => {
              return feature.geometry.type === 'Point';
            });
            newNodes = newNodes.filter((feature) => {
              return !inList(oldDataNodes, feature);
            });

            newData = newData.filter((feature) => {
              return feature.geometry.type !== 'Point';
            });
            newData = newData.filter((feature) => {
              return !inList(oldData, feature);
            });

            if (newNodes.length) {
              this.geoJsonNodesLayer.current.leafletElement.addData(newNodes);
              this.setState({
                dataNodes: [...this.state.dataNodes, ...newNodes],
              });
              gotSomething[1] = true;
            }
            if (newData.length) {
              this.props.unselectPolygon();
              newData.forEach((feature) => {
                // cheat : inverse lat & lng for conformity with polygons overpass output
                const mouseClick = [params.center.lng, params.center.lat];
                const polygonPositions = feature.geometry.coordinates[0];
                if (pointInPolygon(mouseClick, polygonPositions)) {
                  if (!this.context) {
                    this.props.addNotification('notConnected');
                  } else {
                    this.props.handleSelectStart();
                    fetch(`/api/get/${feature.id}`)
                      .then((response) => response.json())
                      .then(
                        (data) => {
                          let bounds = L.latLngBounds(data.positions);
                          bounds = [
                            [bounds._southWest.lat, bounds._southWest.lng],
                            [bounds._northEast.lat, bounds._northEast.lng],
                          ];
                          console.log(bounds);

                          this.props.selectPolygon(data, bounds);
                        },
                        (err) => {
                          this.setState({ err });
                          console.log(err);
                          this.handleFetchEnd(err);
                        }
                      );
                    this.props.handleSelectEnd();
                  }
                }
              });

              this.geoJsonLayer.current.leafletElement
                .clearLayers()
                .addData([...this.state.data, ...newData]);
              this.setState({ data: [...this.state.data, ...newData] });
              gotSomething[0] = true;
            }
          }
          gotSomething[0] ? console.log('got data') : console.log('NO data');
          gotSomething[1] ? console.log('got nodes') : console.log('NO node');
          this.handleFetchEnd();
        },
        (err) => {
          this.setState({ err });
          console.log(err);
          this.handleFetchEnd(err);
        }
      );
  }
  render() {
    const paneStyle =
      this.props.mode == 'trip' ? { zIndex: 'auto' } : { zIndex: 100 };
    return (
      <>
        <>
          <Map.Pane style={paneStyle}>
            <Map.GeoJSON
              ref={this.geoJsonLayer}
              onEachFeature={this.onEachFeature}
              style={this.style}
            ></Map.GeoJSON>
          </Map.Pane>

          <Map.Pane>
            <Map.GeoJSON
              ref={this.geoJsonNodesLayer}
              pointToLayer={this.pointToLayer}
            ></Map.GeoJSON>
          </Map.Pane>
        </>
      </>
    );
  }
}

const mapState = (state, ownProps) => ({
  mode: state.mode,
  marker: state.newDoorMarker,
  selectedPoly: state.selectedPoly,
  nodeOpacity: state.nodeOpacity,
  newDoorOptions: state.newDoorOptions,
  visitedPolys: state.selectedPoly.visitedIds,
  newDoorChangedList: state.newDoor.changedIds,
});

const mapDispatch = (dispatch) => ({
  setTrip: (_) => {
    dispatch.selectedPoly.unselectPolygon();
    dispatch.mode.setMode('trip');
  },
  selectPolygon: (feature, bounds) => {
    dispatch.selectedPoly.selectPolygonWrapped(feature);
    dispatch.polyBounds.setPolyBounds(bounds);
    dispatch.mode.setMode('selectedPoly');
  },
  unselectPolygon: (_) => {
    dispatch.selectedPoly.unselectPolygon();
    dispatch.newDoorMarker.setNewDoorMarker(false);
    dispatch.mode.setMode('trip');
  },
  addNotification: (n) => dispatch.notifications.addNotification(n),
});

SimpleFetcher.propTypes = {
  propagate: PropTypes.func,
  params: PropTypes.object,
  loadFetch: PropTypes.bool,
  loadSelect: PropTypes.bool,
  handleFetchEnd: PropTypes.func,
  handleSelectStart: PropTypes.func,
  handleSelectEnd: PropTypes.func,
  show: PropTypes.bool,
};

export default connect(mapState, mapDispatch)(SimpleFetcher);
