const colors = {
  pink: {
    fillColor: '#ff78bb',
    color: '#ee12bb',
  },
  orange: {
    fillColor: '#ff7800',
    color: '#ee1200',
  },
  blue: {
    fillColor: '#4eb1bb',
    color: '#33a1bb',
  },
  green: {
    fillColor: '#4ed140',
    color: '#ee1255',
  },
};

const mouseOverStyle = {
  fillColor: colors.orange.fillColor,
  color: colors.orange.color,
  fill: true,
  weight: 3,
  opacity: 0.5,
  fillOpacity: 0.5,
  stroke: true,
  display: 'true',
};

const visitedStyle = {
  fillColor: colors.orange.fillColor,
  color: colors.orange.color,
  stroke: true,
  fill: true,
  weight: 2,
  opacity: 0.5,
  fillOpacity: 0.2,
  display: 'true',
};

const modifiedDoorStyle = {
  fillColor: colors.blue.fillColor,
  color: colors.blue.color,
  stroke: true,
  fill: true,
  weight: 2,
  opacity: 0.5,
  fillOpacity: 0.2,
  display: 'true',
};

const defaultStyle = {
  fillColor: colors.pink.fillColor,
  color: colors.pink.color,
  weight: 2,
  stroke: true,
  fill: true,
  opacity: 0.5,
  fillOpacity: 0.1,
  display: 'true',
};

const polygonStyle = {
  mouseOverStyle,
  visitedStyle,
  modifiedDoorStyle,
  defaultStyle,
};

export default polygonStyle;
