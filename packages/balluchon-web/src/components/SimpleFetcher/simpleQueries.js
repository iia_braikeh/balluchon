const buildingQuery = ({ bounds, aroundValue, position }) => `
                [timeout:30][out:json];
                (
                    way[building](${bounds});
                    relation[building](${bounds});
                    way[amenity=parking](${bounds});
                    relation[amenity=parking](${bounds});
                    node[entrance](${bounds});
                    node[door](${bounds});
                );
                out body geom;
                `;
const buildingAndPossibleDoorsQuery = ({ bounds, aroundValue, position }) => `
                [timeout:30][out:json];
                (
                    relation[building](${bounds});
                    relation[amenity=parking](${bounds});
                    node[entrance](${bounds});
                    node[door](${bounds});
                    way[amenity=parking](${bounds});
                    node(w)["addr:housenumber"];
                    way[building](${bounds});
                    node(w)["addr:housenumber"];
                );
                out body geom;
                `;

const building2Query = ({ bounds, aroundValue, position }) => `
                [timeout:30][out:json];
                (
                    way[building](around:${aroundValue},${position});
                    relation[building](around:${aroundValue},${position});
                    way[amenity=parking](around:${aroundValue},${position});
                    relation[amenity=parking](around:${aroundValue},${position});
                    node[entrance](around:${aroundValue},${position});
                    node[door](around:${aroundValue},${position});
                );
                
                out body geom;
                `;

const simpleQueries = {
  buildingQuery,
  building2Query,
  buildingAndPossibleDoorsQuery,
};

export default simpleQueries;
