import React, { Component } from 'react';
import {
  BlueTheme,
  Map,
  Toolbar,
  Notification,
  Modal,
  Loader,
} from 'osm-ui-react';
import Legend from './Legend';
import About from '../pages/About/About';
import SimpleFetcher from './SimpleFetcher/SimpleFetcher';
import Doorbar from './Doorbar/Doorbar';
import Userbar from './Userbar';
import Searchbar from './Searchbar/Searchbar';
import SelectedPoly from './SelectedPoly';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { simulateBox, boundsToRectangle } from '../helpers/geometry';
import { isTouchDevice } from '../helpers/device';
import L from 'leaflet';

const bing_key = process.env.REACT_APP_BING_KEY;
const mapbox_key = process.env.REACT_APP_MAPBOX_KEY;
const layersUrls = [
  'https://{s}.tile.osm.org/{z}/{x}/{y}.png',
  `https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v11/tiles/{z}/{x}/{y}?access_token=${mapbox_key}`,
];

const touchDevice = isTouchDevice();
const Mousetrap = require('mousetrap');

class Od4mMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      debug: false,
      about: false,
      autoZoom: true,
      locate: true,
      locateMeNow: false,
      visitedBounds: [],
      clickPosition: null,
      loadFetch: false,
      loadSelect: false,
      isSwiping: false,
      layer: 0,
      notifications: [],
      zoom: props.zoom,
      center: props.center,
    };
    // this.geoJsonLayer = React.createRef();
    this.mapRef = React.createRef();
    this.locateRef = React.createRef();
  }
  componentDidMount() {
    Mousetrap.bind('esc', () => {
      if (this.state.about) {
        this.closeAbout();
      }
    });
  }
  componentWillUnmount() {
    Mousetrap.unbind('esc');
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.props.mode !== prevProps.mode) {
      this.updateUrl();
    }
    let zoom, lat, lng, center;
    const hash = this.props.location.hash;
    const match = hash.match(/.*map=(\d{1,2})\/(-?\d[0-9.]*)\/(-?\d[0-9.]*)/);
    if (match) {
      zoom = parseInt(match[1]);
      lat = parseFloat(match[2]);
      lng = parseFloat(match[3]);
      lat = +lat.toFixed(5);
      lng = +lng.toFixed(5);
      center = { lat, lng };
      if (
        prevState.zoom !== zoom ||
        Math.abs(prevState.center.lat - center.lat) > 0.00001 ||
        Math.abs(prevState.center.lng - center.lng) > 0.00001
      ) {
        const map = this.mapRef.current.leafletElement;
        map.flyTo(center, zoom);
        // this.setState({center,zoom})
      }
    }
    // autozoom after selecting polygon
    if (this.props.polyBounds !== prevProps.polyBounds) {
      const map = this.mapRef.current.leafletElement;
      const maxZoom =
        window.innerWidth < 800 ? Math.max(18, zoom) : Math.max(19, zoom);
      const paddingTopLeft = window.innerWidth < 800 ? [15, -300] : [400, 0];
      map.fitBounds(this.props.polyBounds, {
        maxZoom,
        paddingTopLeft,
      });
    }
  }
  updateUrl = (push = false) => {
    const { zoom, center } = this.state;
    let { lat, lng } = center;
    lat = +lat.toFixed(5);
    lng = +lng.toFixed(5);
    const details = this.props.selectedPoly.poly
      ? `?selected=${this.props.selectedPoly.id}`
      : '';
    const newUrl = `/${details}#map=${zoom}/${lat}/${lng}`;
    if (push) {
      this.props.history.push(newUrl);
    } else {
      this.props.history.replace(newUrl);
    }
  };
  storeMapPosition = () => {
    const map = this.mapRef.current.leafletElement;
    sessionStorage.setItem('od4m_doors_zoom', map.getZoom());
    const center = map.getCenter();
    sessionStorage.setItem('od4m_doors_lat', center.lat);
    sessionStorage.setItem('od4m_doors_lng', center.lng);
  };
  zoomIn = () => {
    const map = this.mapRef.current.leafletElement;
    map.zoomIn();
  };

  zoomOut = () => {
    const map = this.mapRef.current.leafletElement;
    map.zoomOut();
  };
  changeLayer = () => {
    this.setState({ layer: (this.state.layer + 1) % 2 });
  };

  handleSelectStart = () => {
    this.setState({ loadSelect: true, loading: true });
  };
  handleSelectEnd = () => {
    this.setState({ loadSelect: false, loading: false });
  };
  handleFetchEnd = (err) => {
    this.setState({ loadFetch: false, loading: false });
    if (err) {
      // notify that fetching failed
      console.log('error while fetching : ', err);
      this.props.addNotification('fetchingError');
    } else {
      // success, add bounds to visitedBounds
      this.setState({
        visitedBounds: [...this.state.visitedBounds, this.state.bounds],
      });
    }
  };

  googleView = () => {
    console.log('GOOGLE VIEW');
    const { zoom, center } = this.state;
    const map = this.mapRef.current.leafletElement;
    // if (map.getZoom() < 17) {
    if (zoom < 17) {
      this.props.addNotification('zoom17atleast');
    } else {
      const newBounds = map.getBounds();
      // let center = map.getCenter();
      const clickCenter = [center.lat, center.lng];
      this.setState({
        // center,
        loading: true,
        bounds: newBounds,
        clickPosition: clickCenter,
        loadFetch: true,
        context: 'building',
      });
    }
  };

  insideVisitedBounds = (point) => {
    console.log('testing bounds...');
    if (!this.state.visitedBounds.length) {
      return false;
    }
    const visited = this.state.visitedBounds;
    return visited.some((box) => box.contains(point));
  };

  handleClick = (e) => {
    // prevent click after dragging the map
    if (
      e.target.dragging &&
      e.target.dragging._draggable &&
      e.target.dragging._draggable._endPos
    ) {
      console.log(e.target.dragging._draggable._endPos);
      return false;
    }
    console.log('click');
    switch (this.props.mode) {
      case 'search':
        this.props.setTrip();
        this.selectBuilding(e);
        break;
      case 'trip':
      case 'selectedPoly':
        this.selectBuilding(e);
        break;
      case 'newDoor':
        this.toggleNewDoor(e);
        break;
      default:
    }
  };

  selectBuilding = (e) => {
    const map = this.mapRef.current.leafletElement;
    const zoom = map.getZoom();
    const currentPosition = this.state.currentPosition;
    const insideVisitedBounds = this.insideVisitedBounds(currentPosition);
    console.log(`inside : ${insideVisitedBounds}`);
    if (insideVisitedBounds) {
      // console.log("should select building now");
      console.log('unselect poly');
      this.props.unselectPolygon();
      return false;
    }
    let newBounds;
    if (zoom < 15) {
      // this.setState({ notifications: true });
      console.log('need closer zoom to fetch buildings');
      if (this.state.autoZoom) {
        map.flyTo(currentPosition, zoom + 2);
      }
      return false;
      // avoid this because from the layers events precision is not equal
      // const currentPosition = e.latlng;
    } else if (zoom < 18) {
      newBounds = simulateBox(map, currentPosition, 15);
    } else {
      newBounds = map.getBounds();
    }
    // map.panTo(currentPosition);

    this.setState({
      loading: true,
      clickPosition: this.state.currentPosition,
      bounds: newBounds,
      loadFetch: true,
      context: 'building',
    });
    // this.setState({
    //     loading: true,
    //     // visitedBounds: [...this.state.visitedBounds, newBounds],
    //     clickPosition: currentPosition,
    //     bounds: newBounds,
    //     loadingBuildings: true,
    //     context: "building2"
    // });
  };

  updateCenter = () => {
    if (this.state.locateMeNow) {
      this.setState({ locateMeNow: false });
      setTimeout(() => this.stopLocateMe(), 2000);
    }
    const map = this.mapRef.current.leafletElement;
    const zoom = map.getZoom();
    const center = map.getCenter();
    this.setState({ zoom, center });
    this.updateUrl();
  };
  flyToLocation = (boundingbox) => {
    const corner1 = L.latLng(boundingbox[0], boundingbox[3]),
      corner2 = L.latLng(boundingbox[1], boundingbox[2]);
    const map = this.mapRef.current.leafletElement;
    var target = map._getBoundsCenterZoom([corner1, corner2]);
    map.flyTo(target.center, target.zoom);
  };
  ////////////////////////////
  goToOsm = () => {
    const { zoom, center } = this.state;
    window.open(
      `https://www.openstreetmap.org/#map=${zoom}/${center.lat}/${center.lng}`,
      '_blank'
    );
    return null;
  };
  openAbout = () => {
    this.setState({ about: true });
  };
  closeAbout = () => {
    this.setState({ about: false });
  };

  handleMouseMove = (e) => {
    if (
      (this.props.mode !== 'newDoor' && this.props.mode !== 'changeset') ||
      this.props.newDoorMarker === true
    ) {
      this.setState({ currentPosition: e.latlng });
    }
  };

  toggleNewDoor = (e) => {
    if (!this.props.newDoorMarker) {
      this.setState({ currentPosition: e.latlng });
    }
    this.props.toggleNewDoorMarker();
  };

  ///////////////////////////////
  locateMe = () => {
    this.setState({ locateMeNow: true });
    this.locateRef.current.locateMe();
  };
  stopLocateMe = () => {
    this.locateRef.current.stopLocateMe();
  };
  toggleAutoZoom = () => {
    this.setState((state, props) => ({ autoZoom: !state.autoZoom }));
  };
  toggleSearchbar = () => {
    if (this.props.mode === 'trip') {
      this.props.setSearch();
    } else if (this.props.mode === 'search') {
      this.props.setTrip();
    }
  };
  /////////////////////////////

  render() {
    const {
      debug,
      center,
      zoom,
      locate,
      bounds,
      notifications,
      about,
      clickPosition,
      loading,
      loadFetch,
      loadSelect,
      visitedBounds,
      autoZoom,
      context,
      currentPosition,
      layer,
    } = this.state;
    const params = loadFetch
      ? { context: context, center: clickPosition, bounds: bounds }
      : null;
    // if (this.mapRef.current && this.mapRef.current.leafletElement) console.log(this.mapRef.current);
    const layerUrl = layersUrls[layer];
    return (
      <div>
        {notifications &&
          notifications.map((n, i) => (
            <Notification
              id={i}
              direction={n.direction || 'vertical'}
              position='top'
              timespan={n.timespan || 2000}
            >
              {n.content}
            </Notification>
          ))}
        {/* <BingExample /> */}
        <Map
          center={center}
          zoom={zoom}
          oRef='map'
          ref={this.mapRef}
          locateRef={this.locateRef}
          style={{ height: '100vh' }}
          zoomControl={false}
          onClick={this.handleClick}
          onMouseMove={this.handleMouseMove}
          // onzoomend={this.updateZoom}
          onzoomend={this.updateCenter}
          onmoveend={this.updateCenter}
          locate={locate}
          dragging={this.props.mode !== 'newDoor' || !touchDevice}
          bing={{ bingkey: bing_key }}
        >
          {/* <Map.CachedTileLayer
                        useWorker={true}
                        useCache
                        crossOrigin
                        cacheNextZoomLevel
                        cacheEdgeTile={1}
                        attribution='&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
                    /> */}
          <Map.TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url={layerUrl}
            maxNativeZoom={19}
            minZoom={0}
            maxZoom={22}
          />

          <SelectedPoly
            mapref={this.mapRef.current}
            mousePos={
              this.props.mode === 'newDoor' || this.props.mode === 'changeset'
                ? currentPosition
                : undefined
            }
            radius={
              this.state.zoom > 18 ? this.state.zoom / 1.5 : this.state.zoom / 4
            }
          />
          {debug &&
            visitedBounds.length &&
            visitedBounds.map((bound) => (
              <Map.Rectangle bounds={boundsToRectangle(bound)} fill={false} />
            ))}
          {debug && (
            <Map.Marker
              position={[44.8741429890617, -0.5345040893554689]}
              shape='pointerCirclePin'
              icon='bolt'
            />
          )}
          <SimpleFetcher
            propagate={this.handleClick}
            params={params}
            loadFetch={loadFetch}
            loadSelect={loadSelect}
            handleFetchEnd={this.handleFetchEnd}
            handleSelectStart={this.handleSelectStart}
            handleSelectEnd={this.handleSelectEnd}
            show={zoom > 16}
          />
          <Legend />
        </Map>
        {loading && (
          <Modal>
            <Loader spinnerSize={60} strokeSize={4} />
            {/* <Button onClick={this.closeModal}>Fermer</Button> */}
            {/* <Button onClick={this.cancelRequest}>Annuler</Button> */}
          </Modal>
        )}
        <BlueTheme>
          <Userbar storePosition={this.storeMapPosition} />
        </BlueTheme>
        <Toolbar opened>
          <Toolbar.Group shape='square'>
            <Toolbar.Item icon='plus' onClick={this.zoomIn} />
            <Toolbar.Item icon='minus' onClick={this.zoomOut} />
            <Toolbar.Item inactive>{zoom}</Toolbar.Item>
          </Toolbar.Group>
          <Toolbar.Group>
            <Toolbar.Item
              icon='search'
              shape='square'
              onClick={this.toggleSearchbar}
              label='rechercher un endroit'
              showLabel='outside'
            />
            <Toolbar.Item
              icon='location-arrow'
              shape='square'
              onClick={this.locateMe}
              label='localisez-moi'
              showLabel='outside'
            />
            <Toolbar.Item
              icon='search'
              shape='round'
              onClick={this.toggleAutoZoom}
              label='auto-zoom'
              showLabel='outside'
            >
              {autoZoom ? 'on' : 'off'}
            </Toolbar.Item>
          </Toolbar.Group>
          <Toolbar.Group>
            <Toolbar.Item
              icon='bullseye'
              onClick={this.props.changeNodeOpacity}
              label='affichage des portes et entrées'
              showLabel='outside'
            />
            <Toolbar.Item
              icon='clone'
              shape='square'
              onClick={this.changeLayer}
              label='Fonds de carte'
              showLabel='outside'
            />
          </Toolbar.Group>

          <Toolbar.Item
            icon='home'
            shape='square'
            onClick={this.googleView}
            label='Explorer'
            showLabel='outside'
          />
          <Toolbar.Group>
            <Toolbar.Item
              icon='question'
              shape='square'
              onClick={this.openAbout}
              label='aide / à propos'
              showLabel='outside'
            />
            <Toolbar.Item
              icon='info'
              onClick={this.goToOsm}
              label='voir sur openstreetmap'
              showLabel='outside'
            />
          </Toolbar.Group>
        </Toolbar>
        <About onClose={this.closeAbout} opened={about} />
        <Doorbar mapref={this.mapRef.current} />
        <Searchbar flyToLocation={this.flyToLocation} />
      </div>
    );
  }
}

const MapWithRouter = withRouter(Od4mMap);

const mapState = (state, ownProps) => ({
  mode: state.mode,
  selectedPoly: state.selectedPoly,
  newDoorMarker: state.newDoorMarker,
  polyBounds: state.polyBounds,
});

const mapDispatch = (dispatch) => ({
  setTrip: (_) => {
    dispatch.selectedPoly.unselectPolygon();
    dispatch.mode.setMode('trip');
  },
  setSearch: (_) => {
    dispatch.mode.setMode('search');
  },
  unselectPolygon: (_) => {
    dispatch.selectedPoly.unselectPolygon();
    dispatch.newDoorMarker.setNewDoorMarker(false);
    dispatch.mode.setMode('trip');
  },
  setSelectedPoly: (_) => dispatch.mode.setMode('selectedPoly'),
  changeNodeOpacity: (_) => dispatch.nodeOpacity.changeNodeOpacity(),
  setNewDoorMarkerTrue: (_) => dispatch.newDoorMarker.setNewDoorMarker(true),
  toggleNewDoorMarker: (_) => dispatch.newDoorMarker.toggleNewDoorMarker(),
  addNotification: (n) => dispatch.notifications.addNotification(n),
});

export default connect(mapState, mapDispatch)(MapWithRouter);
