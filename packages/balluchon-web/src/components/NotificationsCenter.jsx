import React from 'react';
import PropTypes from 'prop-types';
import { Notification } from 'osm-ui-react';
import notificate from '../constants/notifications';
import { connect } from 'react-redux';

class NotificationCenter extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        {this.props.notifications.map((key) => {
          clearTimeout(this.notif);
          this.notif = setTimeout(() => {
            this.props.removeNotification(notificate.key);
          }, 5000);
          return (
            <Notification key={key} id={key}>
              {notificate[`${key}`].content}
            </Notification>
          );
        })}
      </div>
    );
  }
}

NotificationCenter.propTypes = {
  children: PropTypes.node.isRequired,
};
NotificationCenter.defaultProps = {};
NotificationCenter.displayName = 'NotificationCenter';

const notifState = (state, ownProps) => ({
  notifications: state.notifications,
});

const notifDispatch = (dispatch) => ({
  removeNotification: (n) => dispatch.notifications.removeNotification(n),
});

export default connect(notifState, notifDispatch)(NotificationCenter);
