import React from 'react';
import { Map } from 'osm-ui-react';
import PropTypes from 'prop-types';

function DoorNode(props) {
  const { center, possibleDoor, selected, onMouseOver, onClick } = props;

  const radius = possibleDoor ? 7 : 5;
  const fillColor = selected
    ? ' #9d1f3d '
    : possibleDoor
    ? ' white '
    : '#118029';
  const color = possibleDoor ? 'green' : '#118029';
  const stroke = possibleDoor ? true : false;

  return (
    <React.Fragment>
      {(onMouseOver || onClick) && (
        <Map.CircleMarker
          center={center}
          radius={radius * 3}
          fillColor={'blue'}
          fillOpacity={selected ? 0.2 : 0}
          stroke={false}
          onMouseOver={onMouseOver}
          onClick={onClick}
          style={{ zIndex: 5000 }}
        />
      )}
      <Map.CircleMarker
        center={center}
        radius={radius}
        fillColor={fillColor}
        color={color}
        fillOpacity={1}
        stroke={stroke}
        onMouseOver={onMouseOver}
        onClick={onClick}
      />
    </React.Fragment>
  );
}

DoorNode.propTypes = {
  center: PropTypes.any.isRequired, // FIX ME
  possibleDoor: PropTypes.bool,
  selected: PropTypes.bool,
  onMouseOver: PropTypes.func,
  onClick: PropTypes.func,
};

export default DoorNode;
