import React from 'react';
import PropTypes from 'prop-types';
import { Editor } from 'osm-ui-react';

const Feature = ({ tags, feature }) => {
  let properties;
  if (tags) {
    properties = Object.entries(tags);
  } else if (feature && feature.properties) {
    properties = Object.entries(feature.properties);
  } else {
    return null;
  }

  return (
    <div>
      <p></p>
      <div
      // style={{ border: "2px solid green", padding: "5px" }}
      >
        {properties.map(([key, value]) => {
          if (value) {
            return (
              <>
                {/* {key !== 'name' && (<li key={key}><strong>{key}</strong> : {value} </li>)} */}
                <Editor.Field tag={key} value={value} />
              </>
            );
          } else {
            return null;
          }
        })}
      </div>
    </div>
  );
};

Feature.propTypes = {
  tags: PropTypes.object, // FIX ME
  feature: PropTypes.object, // FIX ME
};

export default Feature;
