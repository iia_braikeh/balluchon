import React, { Component } from 'react';
import { Form, Button } from 'osm-ui-react';
import PropTypes from 'prop-types';

class Changeset extends Component {
  constructor(props) {
    super(props);
    const message =
      props.type === 'newDoor'
        ? `Ajout d'une porte sur ${
            props.name ? 'le bâtiment "' + props.name + '".' : 'un bâtiment.'
          }`
        : props.type === 'alterDoor'
        ? `Modification d'une porte sur ${
            props.name ? 'le bâtiment "' + props.name + '".' : 'un bâtiment.'
          }`
        : '';

    this.state = {
      message: message,
    };
  }
  handleChange = (event) => {
    this.setState({ message: event.target.value });
  };
  render() {
    const { onSubmit, onRewind } = this.props;
    return (
      <Form>
        {/* <h3>Identification de la nouvelle porte</h3> */}
        {/* <p>Message d’édition</p> */}
        <Form.Textarea onChange={this.handleChange}>
          {this.state.message}
        </Form.Textarea>
        <p>
          <Button onClick={onRewind}>Retour</Button>
          {'  '}
          <Button
            icon='paper-plane'
            onClick={() => onSubmit(this.state.message)}
          >
            Envoyer
          </Button>
        </p>
      </Form>
    );
  }
}

Changeset.propTypes = {
  type: PropTypes.string,
  name: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
};

export default Changeset;
