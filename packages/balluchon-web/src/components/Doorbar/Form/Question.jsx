import React from 'react';
import { Form } from 'osm-ui-react';

const Question = ({ question, id, initialValue, moreOptions, onChange }) => {
  const handleChange = (event) => {
    const value = event ? event.value : '';
    onChange({ key: question.tag, value: value });
  };
  const options = [];
  if (question.options) {
    options.push(...question.options);
  }
  if (moreOptions) {
    moreOptions = moreOptions.map((v) => ({ label: v, value: v }));
    options.push(...moreOptions);
  }

  return (
    <React.Fragment>
      <Form.Label htmlFor={`question${id}`}>{question.question}</Form.Label>
      {question.type === 'select' && (
        <Form.Select
          id={`question${id}`}
          // multi={false}
          // searchable={false}
          disabled={false}
          clearable={true}
          onChange={handleChange}
          options={options}
          initialValue={initialValue}
        />
      )}
      {question.type === 'number' && (
        <Form.Input
          id={`question${id}`}
          type='number'
          style={{ width: '50%' }}
          disabled={false}
          placeholder='nombre'
          clearable={true}
          onChange={handleChange}
          value={initialValue}
        />
      )}
      {question.type === 'text' && (
        <Form.Input
          id={`question${id}`}
          type='text'
          style={question.style || null}
          disabled={false}
          placeholder={question.placeholder || null}
          clearable={true}
          onChange={handleChange}
          value={initialValue}
        />
      )}
    </React.Fragment>
  );
};

export default Question;
