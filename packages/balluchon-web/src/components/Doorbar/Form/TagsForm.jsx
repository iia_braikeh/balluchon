import React from 'react';
import { Form, Button } from 'osm-ui-react';
import Question from './Question';
import PropTypes from 'prop-types';
import questions from '../../../constants/questions';

const TagsForm = ({
  type,
  initialTags,
  moreOptions,
  onChange,
  onRewind,
  onSubmit,
}) => {
  // type : building, parking, park,
  // leisure:dog_park
  // leisure:park
  // leisure:garden

  // amenity:parking
  // parking:surface
  return (
    <Form>
      {questions[type].map((question, id) => (
        <Question
          key={id}
          id={id}
          question={question}
          moreOptions={
            moreOptions && question.moreOptions
              ? moreOptions[`${question.moreOptions}`]
              : null
          }
          onChange={onChange}
          initialValue={initialTags[question.tag]}
        />
      ))}
      <p>
        <Button onClick={onRewind}>Retour</Button>
        {'  '}
        <Button onClick={onSubmit}>Continuer</Button>
      </p>
    </Form>
  );
};

TagsForm.propTypes = {
  type: PropTypes.oneOf(['building', 'parking', 'park', 'doorAddressTags'])
    .isRequired,
  onChange: PropTypes.func,
  onRewind: PropTypes.func,
  onSubmit: PropTypes.func,
};
TagsForm.defaultProps = {
  type: 'building',
};

export default TagsForm;
