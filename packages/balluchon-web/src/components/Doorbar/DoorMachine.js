import { Machine } from 'xstate';

// This machine is completely decoupled from React
export const doorMachine = Machine(
  {
    id: 'doorBar',
    initial: 'trip',
    on: {
      CANCEL: 'trip',
    },
    states: {
      trip: {
        on: {
          SELECT: {
            target: 'selectedPoly',
            // transition actions
            actions: (context, event) => {
              console.log('activating...');
            },
          },
        },
      },
      selectedPoly: {
        on: {
          NEWDOOR: 'newDoor',
        },
      },
      newDoor: {
        on: {
          SELECT: 'selectedPoly',
        },
      },
    },
  }
  // {
  //     actions: {
  //         // action implementations
  //         activate: (context, event) => {
  //             console.log('activating...');
  //         },
  //         notifyActive: (context, event) => {
  //             console.log('active!');
  //         },
  //         notifyInactive: (context, event) => {
  //             console.log('inactive!');
  //         },
  //         sendTelemetry: (context, event) => {
  //             console.log('time:', Date.now());
  //         }
  //     }
  // }
);
