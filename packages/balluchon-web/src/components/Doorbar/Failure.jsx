import React from 'react';
import { Button } from 'osm-ui-react';
import PropTypes from 'prop-types';

function Failure({ message, setTrip }) {
  return (
    <div>
      <h4>Echec de la contribution :(</h4>
      <p>L'envoi de votre contribution a échoué</p>
      <p>
        <em>{message}</em>
      </p>
      <p>Vous pouvez maintenant fermer ce panneau et essayez à nouveau.</p>
      <Button block onClick={setTrip}>
        Fermer le panneau
      </Button>
    </div>
  );
}

Failure.propTypes = {
  message: PropTypes.string,
  setTrip: PropTypes.func.isRequired,
};

export default Failure;
