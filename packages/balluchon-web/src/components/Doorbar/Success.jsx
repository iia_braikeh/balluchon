import React from 'react';
import { Button } from 'osm-ui-react';
import PropTypes from 'prop-types';

function Success(props) {
  return (
    <div>
      <h4>{props.message}</h4>
      <p>Félicitations et merci pour votre contribution :)</p>
      <p>
        Les changements sont dores et déjà dans la base de données, et seront
        bientôt visibles sur openstreetmap.fr
      </p>
      <p>Vous pouvez maintenant fermer ce panneau et continuer à contribuer.</p>
      <Button block onClick={props.setTrip}>
        Fermer le panneau
      </Button>
    </div>
  );
}

Success.propTypes = {
  message: PropTypes.string.isRequired,
  setTrip: PropTypes.func.isRequired,
};

export default Success;
