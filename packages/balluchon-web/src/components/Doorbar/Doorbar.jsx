import React, { Component } from 'react';
import { Sidebar, Button, Loader } from 'osm-ui-react';
import { connect } from 'react-redux';

import FeatureTitle from './FeatureTitle';
import Feature from './Feature';
import TagsForm from './Form/TagsForm';
import Changeset from './Changeset';
import Success from './Success';
import Failure from './Failure';
import { newDoorModesFeatures, alterDoorModesFeatures } from '../../modes';
import { simulateBox, boundsToRectangle } from '../../helpers/geometry';
import { notFullheightDoorbar } from '../../modes';
const Mousetrap = require('mousetrap');

class Doorbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      exploreLoading: false,
      exploreLoaded: false,
      details: true,
    };
  }

  componentDidMount() {
    Mousetrap.bind('esc', () => this.handleGoBack());
  }
  componentWillUnmount() {
    Mousetrap.unbind('esc');
  }

  componentDidUpdate() {
    // cancel fetchExplore if fetching
    // if nexprop of polyId <>old setState({exploreLoading:false,exploreLoaded:false})
  }

  handleGoBack = () => {
    switch (this.props.mode) {
      case 'selectedPoly':
        this.props.setTrip();
        break;
      case 'newDoor':
        this.props.setSelectedPoly();
        break;
      case 'newDoorTags':
        this.props.setBackToNewDoor();
        break;
      case 'newDoorAddress':
        this.props.setNewDoorTags();
        break;
      case 'changeset':
        this.props.setNewDoorAddress();
        break;
      case 'success':
        this.props.setTrip();
        break;
      case 'selectAlterDoor':
        this.props.setSelectedPoly();
        break;
      case 'selectedAlterDoor':
        this.props.setSelectAlterDoor();
        break;
      case 'alterDoorTags':
        this.props.setSelectedAlterDoor();
        break;
      case 'alterDoorAddress':
        this.props.setAlterDoorTags();
        break;
      case 'alterChangeset':
        this.props.setAlterDoorAddress();
        break;
      case 'alterSuccess':
        this.props.setTrip();
        break;
      default:
    }
  };

  handleExplore = () => {
    // fetch localStorage if
    // else IF logged
    // fetchDoors with osmRequest and fetchCache
    // ELSE Notification ;

    // fetchDoors(this.context.poly);
    this.setState({ exploreLoading: true });
  };
  handleMoreDetails = () => {
    this.setState({ details: true });
  };
  handleLessDetails = () => {
    this.setState({ details: false });
  };

  submitNewDoorPoint = () => {
    if (this.props.newDoorMarker === false) {
      this.props.setNewDoorTags();
    } else {
      clearTimeout(this.notif);
      this.props.addNotification('fixDoorMarker');
    }
  };

  submitTagsForm = () => {
    // conditions du formulaire ok : un tag entrance ou un tag door
    const conditions =
      this.props.currentDoorTags.entrance || this.props.currentDoorTags.door;

    if (conditions) {
      this.props.setLoading();
      const newBounds = simulateBox(
        this.props.mapref.leafletElement,
        this.props.currentDoorLatlng,
        16
      );
      console.log(newBounds);
      const d = newBounds._northEast.lat,
        c = newBounds._northEast.lng,
        b = newBounds._southWest.lat,
        a = newBounds._southWest.lng;
      fetch(`/api/get/addr/${a}/${b}/${c}/${d}`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((data) => {
          this.setState({ addressesAround: data });
        });
      this.props.setNewDoorAddress();
    } else {
      clearTimeout(this.notif);
      this.props.addNotification('doorTagsTooLight');
    }
  };

  submitAlterTagsForm = () => {
    // conditions du formulaire ok : un tag entrance ou un tag door
    const conditions =
      this.props.currentAlterDoorTags.entrance ||
      this.props.currentAlterDoorTags.door;

    if (conditions) {
      this.props.setAlterLoading();
      const newBounds = simulateBox(
        this.props.mapref.leafletElement,
        this.props.currentAlterDoorLatlng,
        16
      );
      console.log(newBounds);
      const d = newBounds._northEast.lat,
        c = newBounds._northEast.lng,
        b = newBounds._southWest.lat,
        a = newBounds._southWest.lng;
      fetch(`/api/get/addr/${a}/${b}/${c}/${d}`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((data) => {
          this.setState({ addressesAround: data });
        });
      this.props.setAlterDoorAddress();
    } else {
      clearTimeout(this.notif);
      this.props.addNotification('doorTagsTooLight');
    }
  };

  submitAddressForm = () => {
    const conditions = true;
    if (conditions) {
      const elementHousenumber = this.props.selectedPoly.poly.tags[
        'addr:housenumber'
      ];
      console.log(elementHousenumber);
      if (
        elementHousenumber === this.props.currentDoorTags['addr:housenumber']
      ) {
        console.log(
          'elementHousenumber is already here on the way or relation !'
        );
      }
      this.props.setChangeset();
    } else {
      clearTimeout(this.notif);
      this.props.addNotification('doorTagsTooLight');
    }
  };

  submitAlterAddressForm = () => {
    const conditions = true;
    if (conditions) {
      const elementHousenumber = this.props.selectedPoly.poly.tags[
        'addr:housenumber'
      ];
      console.log(elementHousenumber);
      if (
        elementHousenumber ===
        this.props.currentAlterDoorTags['addr:housenumber']
      ) {
        console.log(
          'elementHousenumber is already here on the way or relation !'
        );
      }
      this.props.setAlterChangeset();
    } else {
      clearTimeout(this.notif);
      this.props.addNotification('doorTagsTooLight');
    }
  };

  submitChangeset = (message) => {
    console.log('sending that new door !');
    this.props.setLoading();

    const body = {
      elementId: this.props.currentDoorElementId,
      wayId: this.props.currentDoorWayId,
      index: this.props.currentDoorIndex,
      wayVersion: this.props.currentAlterDoorVersion,
      newDoorNode: {
        lat: this.props.currentDoorLatlng[0],
        lon: this.props.currentDoorLatlng[1],
        tags: this.props.currentDoorTags,
      },
      newChangeset: {
        createdBy: 'Balluchon - od4m.limos.fr',
        comment: message,
        tags: { locale: 'fr' },
      },
    };
    fetch('/api/changeset', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    }).then((response) => {
      if (response.ok) {
        this.success();
      }
    });
  };

  submitAlterChangeset = (message) => {
    console.log('modifying that door !');
    this.props.setAlterLoading();

    const tags = this.props.currentAlterDoorTags;
    let removedtags = [],
      key;

    for (key in tags) {
      if (tags.hasOwnProperty(key) && tags[key] === '') {
        removedtags.push(key);
      }
    }
    const body = {
      elementId: this.props.currentDoorElementId,
      // wayId: this.props.currentDoorWayId,
      // index: this.props.currentDoorIndex,
      nodeId: this.props.currentAlterDoorId,
      nodeVersion: this.props.currentAlterDoorVersion,
      alterDoorNode: {
        // lat: this.props.currentAlterDoorLatlng[0],
        // lon: this.props.currentAlterDoorLatlng[1],
        tags: this.props.currentAlterDoorTags,
        removedtags: removedtags,
      },
      newChangeset: {
        createdBy: 'Balluchon - od4m.limos.fr',
        comment: message,
        tags: { locale: 'fr' },
      },
    };
    fetch('/api/alterdoorchangeset', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    }).then((response) => {
      if (response.ok) {
        this.alterSuccess();
      } else {
        response.json().then((response) => {
          if (response.errorcode && response.errorcode == 'different version') {
            const errorMessage =
              'La porte ou le bâtiment a été modifié entre temps.';
            this.setState({ errorMessage: errorMessage });
          } else {
            this.setState({ errorMessage: 'mi' });
          }
          this.alterFailure();
        });
      }
    });
  };
  alterSuccess = () => {
    this.props.setAlterSuccess();
  };
  alterFailure = () => {
    this.props.setAlterFailure();
  };

  success = () => {
    this.props.setSuccess();
    this.props.closeNewDoor();
    // this.props.unselectPolygon();
    // autro : ajouter une addresse pour le bâtiment ? (explore)
    //        ajouter une adresse pour la porte (numero, voie)
    //        bouton pour effacer le formulaire
  };

  render() {
    // const { current } = this.state;
    // const { send } = this.service;
    const { history, newDoorOptions } = this.props;
    const { exploreLoaded, exploreLoading, formPage } = this.state;
    const explore = exploreLoaded ? null : exploreLoading ? (
      <Button disabled>loading ...</Button>
    ) : (
      <Button onClick={this.handleExplore}>Explorer les portes</Button>
    );
    const buttons = (
      <>
        {/* {explore} */}
        {/* <Button icon='home' block shape='round' onClick={this.props.setSelectedPoly} /> */}
        {/* <p></p> */}
        {/* <Button icon='external-link-alt'
                    block
                    onClick={this.props.setNewDoor}
                    disabled={this.props.mode === 'newDoor' || this.props.mode === 'changeset'}
                >Nouvelle porte</Button> */}
        <p>
          <Button block size='sm' onClick={this.props.setTrip}>
            Annuler
          </Button>
        </p>
      </>
    );
    const detailsToggler = this.state.details ? (
      <Button
        block
        context='link'
        size='sm'
        icon='plus-square'
        onClick={this.handleLessDetails}
      >
        Masquer les détails
      </Button>
    ) : (
      <Button
        block
        context='link'
        size='sm'
        icon='plus-square'
        onClick={this.handleMoreDetails}
      >
        Afficher les détails
      </Button>
    );
    const detailsFields =
      (this.props.mode === 'selectedPoly' && this.props.selectedPoly.poly && (
        <Feature tags={this.props.selectedPoly.poly.tags} />
      )) ||
      (newDoorModesFeatures.includes(this.props.mode) &&
        this.props.currentDoorElementId && (
          <Feature tags={this.props.currentDoorTags} />
        )) ||
      (alterDoorModesFeatures.includes(this.props.mode) &&
        this.props.currentAlterDoorTags && (
          <Feature tags={this.props.currentAlterDoorTags} />
        ));
    const title = this.props.selectedPoly.poly && (
      <>
        <FeatureTitle tags={this.props.selectedPoly.poly.tags} />
        {/* {detailsToggler} */}
      </>
    );
    let magneticMessage;
    console.log(newDoorOptions.magneticDistance);
    switch (newDoorOptions.magneticDistance) {
      case -1:
        magneticMessage = 'désactivé';
        break;
      case 2:
        magneticMessage = 'faible';
        break;
      case 5:
        magneticMessage = 'moyen';
        break;
      case 8:
        magneticMessage = 'fort';
        break;
      default:
    }
    const helpers = (
      <div>
        <p>
          <Button
            icon='draw-polygon'
            shape='round'
            // size="sm"
            onClick={this.props.changeMagnet}
          >
            Points "collants" : {magneticMessage}
          </Button>
        </p>
        {/* <p>
          <Button
            icon='dove'
            shape='round'
            // size="sm"
            onClick={this.props.toggleAllowInside}
            disabled={true}
          >
            Porte dans l'empreinte du bâtiment
          </Button>
        </p> */}
      </div>
    );

    return (
      <Sidebar
        tinyHeader={window.innerWidth < 800 || window.innerHeight < 500}
        tinyFooter={window.innerWidth < 800 || window.innerHeight < 500}
        // title="A sidebar title"
        scrollContent
        header={title}
        // footer={<Button block onClick={() => unselectPoly()}>Annuler</Button>}
        width={window.innerWidth < 1500 ? 'md' : 'lg'}
        stickbottom
        fullheight={
          !notFullheightDoorbar.includes(this.props.mode) ||
          window.innerWidth >= 800
        }
        maximized={window.innerWidth < 800}
        opened={this.props.mode !== 'trip' && this.props.mode !== 'search'}
        onClickBack={this.handleGoBack}
        onClickClose={this.props.setTrip}
      >
        {this.props.mode === 'selectedPoly' && this.props.selectedPoly.poly && (
          <div>
            <Button
              icon='external-link-alt'
              block
              onClick={this.props.setNewDoor}
              disabled={
                this.props.mode === 'newDoor' || this.props.mode === 'changeset'
              }
            >
              Ajouter une porte
            </Button>
            <p></p>
            <Button
              icon='external-link-alt'
              block
              onClick={this.props.setSelectAlterDoor}
              disabled={!this.props.selectedPoly.poly.hasDoors}
            >
              Modifier les détails d'une porte
            </Button>
            <p></p>
            {detailsToggler}
            {this.state.details && detailsFields}
          </div>
        )}
        {this.props.mode === 'newDoor' && (
          <div>
            <h4>Placez la nouvelle porte (1/4)</h4>
            {helpers}
            <p>
              <Button onClick={this.props.setSelectedPoly}>Retour</Button>
              {'  '}
              <Button onClick={this.submitNewDoorPoint}>Continuer</Button>
            </p>
          </div>
        )}
        {this.props.mode === 'selectAlterDoor' && (
          <div>
            <h4>Cliquez sur une porte à modifier</h4>
            <p>
              <Button onClick={this.props.setSelectedPoly}>Retour</Button>
            </p>
            {detailsToggler}
            {this.state.details && detailsFields}
          </div>
        )}
        {this.props.mode === 'selectedAlterDoor' && (
          <div>
            <Button
              icon='external-link-alt'
              block
              onClick={this.props.setAlterDoorTags}
            >
              Modifier les détails de la porte
            </Button>
            <p></p>
            <Button
              icon='external-link-alt'
              block
              // onClick={' '}
              disabled={true}
            >
              Modifier la position de la porte
            </Button>
            <p></p>
            <Button onClick={this.props.setSelectAlterDoor}>
              Choisir une autre porte
            </Button>
            <p></p>
            {detailsToggler}
            {this.state.details && detailsFields}
          </div>
        )}
        {this.props.mode === 'alterDoorTags' && (
          <div>
            <h4>Modification de la porte (1/3)</h4>
            <TagsForm
              type='doorOfBuildingTags'
              initialTags={this.props.currentAlterDoorTags}
              onChange={this.props.addAlterDoorTag}
              onRewind={this.props.setSelectedAlterDoor}
              onSubmit={this.submitAlterTagsForm}
            />
            <p></p>
            {detailsToggler}
            {this.state.details && detailsFields}
          </div>
        )}
        {this.props.mode === 'alterDoorAddress' && (
          <div>
            <h4>Modification de l'adresse de la porte (2/3)</h4>
            <TagsForm
              type='doorAddressTags'
              initialTags={this.props.currentAlterDoorTags}
              moreOptions={this.state.addressesAround}
              onChange={this.props.addAlterDoorAddress}
              onRewind={this.props.setAlterDoorTags}
              onSubmit={this.submitAlterAddressForm}
            />
            <p></p>
            {detailsToggler}
            {this.state.details && detailsFields}
          </div>
        )}
        {this.props.mode === 'alterChangeset' && (
          <div>
            <h4>Message de validation (3/3)</h4>
            <p>
              <em>modifier si nécessaire</em>
            </p>
            <Changeset
              type={'alterDoor'}
              name={this.props.selectedPoly.poly.tags.name}
              onRewind={this.props.setAlterDoorAddress}
              onSubmit={this.submitAlterChangeset}
            />
            <p></p>
            {detailsToggler}
            {this.state.details && detailsFields}
          </div>
        )}
        {this.props.mode === 'alterSuccess' && (
          <Success
            message={'Porte modifiée avec succès'}
            setTrip={this.props.setTrip}
          />
        )}
        {this.props.mode === 'alterFailure' && (
          <Failure
            message={this.state.errorMessage}
            setTrip={this.props.setTrip}
          />
        )}

        {this.props.mode === 'newDoorTags' && (
          <div>
            <h4>Détails de la nouvelle porte (2/4)</h4>
            <TagsForm
              type='doorOfBuildingTags'
              initialTags={this.props.currentDoorTags}
              onChange={this.props.addNewDoorTag}
              onRewind={this.props.setBackToNewDoor}
              onSubmit={this.submitTagsForm}
            />
            <p></p>
            {detailsToggler}
            {this.state.details && detailsFields}
          </div>
        )}
        {this.props.mode === 'newDoorAddress' && (
          <div>
            <h4>Adresse de la nouvelle porte (3/4)</h4>
            <TagsForm
              type='doorAddressTags'
              initialTags={this.props.currentDoorTags}
              moreOptions={this.state.addressesAround}
              onChange={this.props.addNewDoorAddress}
              onRewind={this.props.setNewDoorTags}
              onSubmit={this.submitAddressForm}
            />
            <p></p>
            {detailsToggler}
            {this.state.details && detailsFields}
          </div>
        )}
        {this.props.mode === 'changeset' && (
          <div>
            <h4>Message de validation (4/4)</h4>
            <p>
              <em>modifier si nécessaire</em>
            </p>
            <Changeset
              type={'newDoor'}
              name={this.props.selectedPoly.poly.tags.name}
              onRewind={this.props.setNewDoorAddress}
              onSubmit={this.submitChangeset}
            />
            <p></p>
            {detailsToggler}
            {this.state.details && detailsFields}
          </div>
        )}
        {(this.props.mode === 'loading' ||
          this.props.mode === 'alterLoading') && (
          <div>
            <p></p>
            <p></p>
            <p></p>
            <Loader />
          </div>
        )}
        {this.props.mode === 'success' && (
          <Success
            message={'Nouvelle porte ajoutée avec succès'}
            setTrip={this.props.setTrip}
          />
        )}
      </Sidebar>
    );
  }
}

const mapState = (state, ownProps) => ({
  mode: state.mode,
  selectedPoly: state.selectedPoly,
  currentDoorLatlng: state.newDoor.currentProps.latlng,
  currentDoorTags: {
    ...state.newDoor.currentProps.tags,
    ...state.newDoor.currentProps.address,
  },
  currentDoorIndex: state.newDoor.currentProps.index,
  currentDoorWayId: state.newDoor.currentProps.wayId,
  currentDoorElementId: state.newDoor.currentElementId,

  currentAlterDoorLatlng: state.newAlterDoor.currentNodeNewProps.latlng,
  currentAlterDoorTags: {
    ...state.newAlterDoor.currentNodeNewProps.tags,
    ...state.newAlterDoor.currentNodeNewProps.address,
  },
  currentAlterOldDoorTags: {
    ...state.newAlterDoor.currentNodeOldProps.tags,
    ...state.newAlterDoor.currentNodeOldProps.address,
  },
  currentAlterDoorId: state.newAlterDoor.currentNodeId,
  currentAlterDoorVersion: state.newAlterDoor.currentNodeNewProps.version,
  newDoorMarker: state.newDoorMarker,
  newDoorOptions: state.newDoorOptions,
});

const mapDispatch = (dispatch) => ({
  setChangeset: (_) => dispatch.mode.setMode('changeset'),
  setLoading: (_) => dispatch.mode.setMode('loading'),
  setSuccess: (_) => dispatch.mode.setMode('success'),
  setNewDoorAddress: (_) => dispatch.mode.setMode('newDoorAddress'),
  setNewDoorTags: (_) => dispatch.mode.setMode('newDoorTags'),
  addNewDoorTag: (tag) => dispatch.newDoor.newDoorTag(tag),
  addNewDoorAddress: (tag) => dispatch.newDoor.newDoorAddressTag(tag),
  setBackToNewDoor: (_) => dispatch.mode.setMode('newDoor'),
  setNewDoor: (_) => {
    dispatch.newDoorMarker.setNewDoorMarkerFirst();
    dispatch.mode.setMode('newDoor');
  },
  setSelectAlterDoor: (_) => dispatch.mode.setMode('selectAlterDoor'),
  setSelectedAlterDoor: (_) => dispatch.mode.setMode('selectedAlterDoor'),
  setAlterChangeset: (_) => dispatch.mode.setMode('alterChangeset'),
  setAlterLoading: (_) => dispatch.mode.setMode('alterLoading'),
  setAlterSuccess: (_) => dispatch.mode.setMode('alterSuccess'),
  setAlterFailure: (_) => dispatch.mode.setMode('alterFailure'),
  setAlterDoorAddress: (_) => dispatch.mode.setMode('alterDoorAddress'),
  setAlterDoorTags: (_) => dispatch.mode.setMode('alterDoorTags'),
  addAlterDoorTag: (tag) => dispatch.newAlterDoor.alterDoorTag(tag),
  addAlterDoorAddress: (tag) => dispatch.newAlterDoor.alterDoorAddressTag(tag),

  closeNewDoor: (_) => {
    dispatch.newDoor.savedChangedEraseProps();
    dispatch.newDoorMarker.setNewDoorMarker(false);
  },
  unselectPolygon: (_) => dispatch.selectedPoly.unselectPolygon(),
  setSelectedPoly: (_) => dispatch.mode.setMode('selectedPoly'),
  setTrip: (_) => {
    dispatch.mode.setMode('trip');
    dispatch.selectedPoly.unselectPolygon();
  },
  changeMagnet: (_) => dispatch.newDoorOptions.changeMagnet(),
  toggleAllowInside: (_) => dispatch.newDoorOptions.toggleAllowInside(),
  addNotification: (n) => dispatch.notifications.addNotification(n),
});

export default connect(mapState, mapDispatch)(Doorbar);
