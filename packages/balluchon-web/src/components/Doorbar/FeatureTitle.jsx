import React from 'react';
import PropTypes from 'prop-types';

function addressToStringReducer(address, item) {
  let nextAddress = address
    ? item
      ? address + ', ' + item
      : address
    : item
    ? item
    : null;
  return nextAddress;
}

function getAddress(tags) {
  const housenumber = tags['addr:housenumber'];
  const street = tags['addr:street'];
  const city = tags['addr:city'];
  const address = [housenumber, street, city];
  return address.reduce(addressToStringReducer);
}

const FeatureTitle = ({ tags, feature }) => {
  let name, address;
  if (tags) {
    name = tags.name || 'Bâtiment sans nom';
    address = getAddress(tags);
  } else if (feature && feature.properties) {
    name = feature.properties.name || 'Bâtiment sans nom';
    address = getAddress(feature.properties);
  } else {
    return null;
  }

  return (
    <>
      {name}
      <br />
      {address}
    </>
  );
};

FeatureTitle.propTypes = {
  tags: PropTypes.object, // FIX ME
  feature: PropTypes.object, // FIX ME
};

export default FeatureTitle;
