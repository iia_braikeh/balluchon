import React from 'react';
import renderer from 'react-test-renderer';

import Changeset from './Changeset';

describe('Our first snapshot test', () => {
  it('Should compare the component with a snapshot', () => {
    const onsubmitfn = jest.fn((x) => true);
    const tree = renderer.create(<Changeset onSubmit={onsubmitfn} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
