import React, { useContext } from 'react';
import { Toolbar } from 'osm-ui-react';
import { RedTheme, GreenTheme } from 'osm-ui-react';
import UserProvider from '../contexts/UserProvider';

const logout = () => {
  // window.location.href = '/auth/logout';
  window.open('/auth/logout', '_self');
  // maybe can add spinner while loading
  return null;
};
const login = () => {
  // window.location.href = '/auth/logout';
  window.open('/auth/openstreetmap', '_self');
  // maybe can add spinner while loading
  return null;
};

const ConditionalWrapper = ({ condition, wrapper, children }) =>
  condition ? wrapper(children) : children;

const LoginItem = ({ storePosition }) => (
  <Toolbar.Item
    icon='user'
    position='top-right'
    label='se connecter'
    showLabel='outside'
    inGroup={true}
    onClick={() => {
      storePosition();
      login();
    }}
  />
);
const LogoutItem = ({ storePosition }) => (
  <RedTheme>
    <Toolbar.Item
      icon='sign-out-alt'
      position='top-right'
      label='se déconnecter'
      showLabel='outside'
      inGroup={true}
      onClick={() => {
        storePosition();
        logout();
      }}
    />
  </RedTheme>
);

const UserMenu = () => (
  <Toolbar.Collapse opened position='top-right'>
    <LogoutItem />
    <Toolbar.Item icon='user' onClick=''></Toolbar.Item>
    <Toolbar.Item
      icon='info'
      label='à propos'
      showLabel='outside'
      onClick=''
    ></Toolbar.Item>
  </Toolbar.Collapse>
);

const Userbar = ({ storePosition }) => {
  const user = useContext(UserProvider.context);
  return (
    <Toolbar opened position='top-right'>
      <ConditionalWrapper
        condition={user}
        wrapper={(children) => <GreenTheme>{children}</GreenTheme>}
      >
        {user ? (
          <LogoutItem storePosition={storePosition} />
        ) : (
          <LoginItem storePosition={storePosition} />
        )}
      </ConditionalWrapper>
    </Toolbar>
  );
};

Userbar.displayName = 'Userbar';

export default Userbar;
