import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Sidebar, Button, Loader, List, Form } from 'osm-ui-react';
import { connect } from 'react-redux';
// import * as Nominatim from 'nominatim-browser';

const Mousetrap = require('mousetrap');

async function fetchStory(signal, query = '') {
  const storyResponse = await fetch(
    `https://nominatim.openstreetmap.org/search?q=${query}&format=json`,
    {
      // photon.komoot.io/api/?q=${query}
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      // cors:true,
      signal: signal,
    }
  );
  const data = await storyResponse.json();
  return data;
}

class Searchbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userQuery: '',
      results: [],
      loading: false,
      fetched: false,
      searchController: null,
      error: false,
    };
    this.textInput = React.createRef();
  }
  componentDidMount() {
    Mousetrap.bind('esc', () => {
      this.props.setTrip();
    });
  }
  componentDidUpdate() {
    if (this.props.mode === 'search') {
      this.textInput.current.focus();
    }
  }
  componentWillUnmount() {
    Mousetrap.unbind('esc');
  }

  closeSearchbar = () => {
    if (this.state.searchController) {
      this.state.searchController.abort();
      this.setState({ loading: false });
    }
    if (this.state.fetched && this.state.results.length === 0) {
      this.setState({ fetched: false });
    }
    this.props.setTrip();
  };
  onKeyUp = (event) => {
    console.log(event.key);
    if (event.key === 'Enter') {
      // Validate
      this.searchLocation();
    } else if (event.key === 'Escape') {
      this.closeSearchbar();
    }
  };
  onChange = (event) => {
    const query = event.target.value;
    this.setState({ userQuery: query });
  };
  searchLocation = () => {
    const query = this.state.userQuery;
    this.setState({ loading: true });
    if (this.state.searchController) {
      this.state.searchController.abort();
    }
    const searchController = new AbortController();
    const signal = searchController.signal;
    this.setState({ searchController });
    fetchStory(signal, query)
      .then((results) => {
        console.log(`Fetch complete. (Not aborted)`);
        this.setState({
          results: results,
          fetched: true,
          loading: false,
          searchController: null,
        });
      })
      .catch((error) => {
        console.error(error);
        this.setState({ error: true, loading: false });
      });
    //         // console.log(result.lat);          // '44.9772995'
    //         // console.log(result.lon);          // '-93.2654691'
    //         // console.log(result.display_name); // 'Minneapolis, Hennepin County, Minnesota, United States of America'

    //         // // result.address is only returned when 'addressdetails: true' is sent in the geocode request
    //         // console.log(result.address.city);    // 'Minneapolis'
    //         // console.log(result.address.county);  // 'Hennepin County'
    //         // console.log(result.address.state);   // 'Minnesota'
    //         // console.log(result.address.country); // 'United States of America'
  };
  goToLocation = (id) => {
    const location = this.state.results[id];
    this.props.flyToLocation(location.boundingbox);
    this.closeSearchbar();
  };

  render() {
    const { loading, fetched, error, results } = this.state;
    const header = (
      <>
        <p>Rechercher un endroit</p>
        <div style={{ display: 'flex' }}>
          <Form.Input
            type='text'
            innerRef={this.textInput}
            placeholder='Gijon, Espagne'
            clearable={true}
            style={{ 'flex-grow': '1' }}
            onChange={this.onChange}
            onKeyUp={this.onKeyUp}
            value={this.state.userQuery}
            // style={{'width':'80%'}}
          />
          <Button context='link' onClick={this.searchLocation} size='sm'>
            OK
          </Button>
        </div>
      </>
    );

    return (
      <div>
        <Sidebar
          header={header}
          // footer={<Button block onClick={() => this.props.setTrip()}>Annuler</Button>}
          width={window.innerWidth < 1000 ? 'md' : 'lg'}
          maximized={window.innerWidth < 800}
          fullheight={results.length > 2}
          stickbottom
          opened={this.props.mode === 'search'}
          onClickClose={this.closeSearchbar}
        >
          {loading && <Loader />}
          {error && (
            <p>
              <em>Une erreur s'est produite. Ré-essayez.</em>
            </p>
          )}
          {!loading &&
            fetched &&
            (results.length <= 0 ? (
              <p>
                <em>Pas de résultat</em>
              </p>
            ) : (
              <List>
                {results.map((r, i) => (
                  <List.Item
                    key={i}
                    isLink
                    onClick={() => this.goToLocation(i)}
                  >
                    {r.display_name}
                  </List.Item>
                ))}
              </List>
            ))}
          {/* {results.length > 2 &&
                        <div style={{ 'height': '100vh' }}></div>} */}
        </Sidebar>
      </div>
    );
  }
}

const mapState = (state, ownProps) => ({
  mode: state.mode,
});

const mapDispatch = (dispatch) => ({
  setTrip: (_) => {
    dispatch.mode.setMode('trip');
    dispatch.selectedPoly.unselectPolygon();
  },
});

Searchbar.propTypes = {
  flyToLocation: PropTypes.func,
};

export default connect(mapState, mapDispatch)(Searchbar);
