export const newDoorModesAlong = [
  'newDoor',
  'newDoorTags',
  'newDoorAddress',
  'changeset',
  'loading',
  'success',
];
export const newDoorModesNodes = [
  'selectedPoly',
  'newDoor',
  'newDoorTags',
  'newDoorAddress',
  'changeset',
  'loading',
  'success',
];
export const newDoorModesFeatures = [
  'newDoor',
  'newDoorTags',
  'newDoorAddress',
  'changeset',
  'loading',
  'success',
];

export const alterDoorModesFeatures = [
  'selectAlterDoor',
  'selectedAlterDoor',
  'alterDoorTags',
  'alterDoorAddress',
  'alterChangeset',
  'alterLoading',
  'loadingChangeset',
  'alterSuccess',
];
export const alterDoorModesNodes = [
  'selectedAlterDoor',
  'alterDoorTags',
  'alterDoorAddress',
  'alterChangeset',
  'alterLoading',
  'loadingChangeset',
  'alterSuccess',
];
export const notFullheightDoorbar = [
  'selectedPoly',
  'newDoor',
  'selectAlterDoor',
  'alterChangeset',
  'alterLoading',
  'loadingChangeset',
  'alterSuccess',
];
