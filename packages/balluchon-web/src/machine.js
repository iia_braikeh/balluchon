import { Machine } from 'xstate';

const newDoorStates = {
  id: 'newDoor',
  initial: 'form',
  states: {
    form: {
      on: { SUBMIT: 'changeset' },
    },
    changeset: {
      on: {
        BACK: 'form',
        SUBMIT: 'submit',
      },
    },
    submit: {
      on: {
        SUCCESS: 'success',
        FAIL: 'changeset',
      },
    },
    success: {},
  },
};
const newNoteStates = {
  id: 'newNote',
  initial: 'form',
  states: {
    form: {
      on: { SUBMIT: 'changeset' },
    },
    changeset: {
      on: {
        BACK: 'form',
        SUBMIT: 'submit',
      },
    },
    submit: {
      on: {
        SUCCESS: 'success',
        FAIL: 'changeset',
      },
    },
    success: {},
  },
};
const exploreStates = {
  id: 'explore',
  initial: 'showDetails',
  states: {
    showDetails: {},
  },
};

// This machine is completely decoupled from React
export const toggleMachine = Machine({
  id: 'modes',
  initial: 'trip',
  states: {
    trip: {
      on: { CLICK: 'loadingData' },
    },
    loadingData: {
      on: { LOADED: 'polyMenu' },
    },
    polyMenu: {
      on: {
        NEWDOOR: 'newDoor',
        EXPLORE: 'explore',
        NEWNOTE: 'putNote',
      },
    },
    newDoor: { ...newDoorStates },
    explore: { ...exploreStates },
    putNote: { ...newNoteStates },
  },
  on: { CANCEL: '.trip' },
});
