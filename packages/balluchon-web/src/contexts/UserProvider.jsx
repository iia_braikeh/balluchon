import React, { createContext, useState, useEffect } from 'react';
const context = createContext(null);

const UserProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  useEffect(
    () => {
      fetch('/api/user', {
        // method: 'cors'
        // credentials: "include"
      })
        .then((res) => res.json())
        .then((result) => {
          setUser(result);
        })
        .catch((err) => {
          console.log(err);
        });
      console.log(user);
    },
    [] // que si .. a changé ; equivaut à component did mount du coup ? car on update jamais ici. ah si, si déconnection
  );
  return <context.Provider value={user}>{children}</context.Provider>;
};
UserProvider.context = context;

export default UserProvider;
