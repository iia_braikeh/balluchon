export const mode = {
  state: 'trip', // initial state
  reducers: {
    // handle state changes with pure functions
    setMode(state, payload) {
      return payload;
    },
  },
};

export const nodeOpacity = {
  state: 0.6, // initial state
  reducers: {
    changeNodeOpacity(state, payload) {
      if (payload) return payload;
      return Math.max(0.2, ((state * 10 + 2) % 10) / 10);
    },
  },
};

export const newDoorOptions = {
  state: {
    magneticDistance: 2,
    allowInside: false,
  }, // initial state
  reducers: {
    changeMagnet(state, payload) {
      if (payload) return { ...state, magneticDistance: payload };
      return {
        ...state,
        magneticDistance: Math.max(-1, ((state.magneticDistance + 5) % 12) - 2),
      };
    },
    toggleAllowInside(state, payload) {
      return { ...state, allowInside: !state.allowInside };
    },
  },
};

// && (Date.now()-state.byElementId.elementId.date)<60000
export const selectedPoly = {
  state: {
    poly: null,
    id: null,
    visitedIds: [],
  }, // initial state
  reducers: {
    selectPolygon(state, feature) {
      return {
        poly: feature,
        id: feature.id,
        visitedIds: [...state.visitedIds, feature.id],
      };
    },
    unselectPolygon(state, payload) {
      return {
        ...state,
        poly: null,
      };
    },
  },
  effects: (dispatch) => ({
    async selectPolygonWrapped(feature, rootState) {
      if (rootState.selectedPoly.id !== feature.id) {
        await dispatch.newDoor.saveDoor(); // dispatch action from a different model
        await dispatch.newDoor.openDoor(feature.id); // dispatch action from a different model
      }
      this.selectPolygon(feature); // dispatch action from this model
    },
  }),
};

export const newDoorMarker = {
  state: false,
  reducers: {
    toggleNewDoorMarker(state, payload) {
      return !state;
    },
    setNewDoorMarker(state, payload) {
      return payload;
    },
  },
  effects: (dispatch) => ({
    async setNewDoorMarkerFirst(payload, rootState) {
      if (
        !rootState.newDoor.currentProps.latlng ||
        rootState.newDoor.currentProps.latlng == null
      ) {
        this.setNewDoorMarker(true);
      } else {
        this.setNewDoorMarker(false);
      }
    },
  }),
};

export const polyBounds = {
  state: false,
  reducers: {
    setPolyBounds(state, payload) {
      return payload;
    },
  },
};

const initialNewDoorProps = {
  latlng: null,
  tags: {},
  address: {},
  index: null,
  wayId: null,
  // "markermoves":true,
};
export const newDoor = {
  state: {
    currentElementId: null,
    currentProps: {},
    byElementId: {},
    allIds: [],
    changedIds: [],
  },
  reducers: {
    openDoor(state, elementId) {
      if (state.allIds.includes(elementId)) {
        return {
          ...state,
          currentElementId: elementId,
          currentProps: state.byElementId[`${elementId}`],
        };
      } else {
        return {
          ...state,
          currentElementId: elementId,
          currentProps: initialNewDoorProps,
        };
      }
    },
    newDoorLatlng(state, latlng) {
      return {
        ...state,
        currentProps: {
          ...state.currentProps,
          latlng: latlng,
        },
      };
    },
    newDoorTag(state, tag) {
      return {
        ...state,
        currentProps: {
          ...state.currentProps,
          tags: {
            ...state.currentProps.tags,
            [`${tag.key}`]: tag.value,
          },
        },
      };
    },
    newDoorAddressTag(state, tag) {
      return {
        ...state,
        currentProps: {
          ...state.currentProps,
          address: {
            ...state.currentProps.address,
            [`${tag.key}`]: tag.value,
          },
        },
      };
    },
    newDoorSegment(state, { wayId, index }) {
      return {
        ...state,
        currentProps: {
          ...state.currentProps,
          index: index,
          wayId: wayId,
        },
      };
    },
    saveDoor(state, payload) {
      if (state.currentProps !== initialNewDoorProps) {
        return {
          ...state,
          currentElementId: null,
          byElementId: {
            ...state.byElementId,
            [`${state.currentElementId}`]: state.currentProps,
          },
          allIds: [...state.allIds, state.currentElementId],
        };
      } else {
        return state;
      }
    },
    savedChangedEraseProps(state, payload) {
      return {
        ...state,
        currentProps: initialNewDoorProps,
        changedIds: [...state.changedIds, state.currentElementId],
      };
    },
  },
};

export const newAlterDoor = {
  state: {
    currentElementId: null,
    currentNodeId: null,
    currentNodeOldProps: {},
    currentNodeNewProps: {},
    // byElementId: {},
    // allIds: [],
  },
  reducers: {
    alterDoor(state, { elementId, nodeId, nodeProps }) {
      // if (state.allIds.includes(elementId)) {
      //     return {
      //         ...state,
      //         currentElementId: elementId,
      //         currentProps: state.byElementId[`${elementId}`],
      //     }
      // } else {
      return {
        ...state,
        currentElementId: elementId,
        currentNodeId: nodeId,
        currentNodeOldProps: nodeProps,
        currentNodeNewProps: nodeProps,
        // }
      };
    },
    // alterDoorLatlng(state, latlng) {
    //     return {
    //         ...state,
    //         currentNodeNewProps: {
    //             ...state.currentNodeNewProps,
    //             "latlng": latlng,
    //         }
    //     }
    // },
    alterDoorTag(state, tag) {
      return {
        ...state,
        currentNodeNewProps: {
          ...state.currentNodeNewProps,
          tags: {
            ...state.currentNodeNewProps.tags,
            [`${tag.key}`]: tag.value,
          },
        },
      };
    },
    alterDoorAddressTag(state, tag) {
      return {
        ...state,
        currentNodeNewProps: {
          ...state.currentNodeNewProps,
          address: {
            ...state.currentNodeNewProps.address,
            [`${tag.key}`]: tag.value,
          },
        },
      };
    },
    // alterDoorSegment(state, { wayId, index, updateWay }) {
    //     return {
    //         ...state,
    //         currentNodeNewProps: {
    //             ...state.currentNodeNewProps,
    //             "index": index,
    //             "wayId": wayId,
    //             "updateWay": updateWay,
    //         }
    //     }
    // },
    // saveDoor(state, payload) {
    //     if (state.currentNodeNewProps !== state.currentNodeOldProps) {
    //         return {
    //             ...state,
    //             currentElementId: null,
    //             byElementId: {
    //                 ...state.byElementId,
    //                 [`${state.currentElementId}`]: state.currentNodeNewProps,
    //             },
    //             allIds: [
    //                 ...state.allIds,
    //                 state.currentElementId
    //             ]
    //         }
    //     }
    //     else {
    //         return state;
    //     }
    // },
    // eraseNewDoorProps(state, payload) {
    //     return {
    //         ...state,
    //         currentNodeNewProps: state.currentNodeOldProps,
    //     }
    // },
  },
};

export const notifications = {
  state: [], // initial state
  reducers: {
    addNotification(state, idToAdd) {
      if (state.includes(idToAdd)) {
        return state;
      }
      return [...state, idToAdd];
    },
    removeNotification(state, idToRemove) {
      return state.filter((n) => n.id !== idToRemove);
    },
  },
};
