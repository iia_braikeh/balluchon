import React from 'react';
import { DefaultTheme, Titlebar } from 'osm-ui-react';
import Od4mMap from '../components/Map';
import NotificationCenter from '../components/NotificationsCenter';
import { withRouter } from 'react-router';
import logo from '../assets/logo/balluchon-150.png';

class Home extends React.PureComponent {
  constructor(props) {
    super(props);
    const comeback = this.props.location.pathname === '/comeback';
    if (comeback) {
      const zoom = parseInt(sessionStorage.getItem('od4m_doors_zoom'));
      let lat = parseFloat(sessionStorage.getItem('od4m_doors_lat'));
      let lng = parseFloat(sessionStorage.getItem('od4m_doors_lng'));
      lat = +lat.toFixed(5);
      lng = +lng.toFixed(5);
      this.state = { zoom, center: { lat, lng } };
      this.props.history.replace(`/#map=${zoom}/${lat}/${lng}`);
    } else {
      let zoom = 10,
        lat = 43,
        lng = -5;
      const hash = this.props.location.hash;
      const match = hash.match(/.*map=(\d{1,2})\/(-?\d[0-9.]*)\/(-?\d[0-9.]*)/);
      if (match) {
        zoom = parseInt(match[1]);
        lat = parseFloat(match[2]);
        lng = parseFloat(match[3]);
        lat = +lat.toFixed(5);
        lng = +lng.toFixed(5);
      }
      this.state = {
        zoom,
        center: { lat, lng },
      };
    }
  }

  render() {
    const { zoom, center } = this.state;
    return (
      <DefaultTheme>
        <Titlebar block={false} smallPadding round>
          <img
            src={logo}
            style={{
              display: 'inline-block',
              'vertical-align': 'middle',
              width: '2rem',
              height: '2rem',
              margin: '0 1rem 0 0',
              padding: '0',
            }}
            alt='logo Balluchon : sac-à-dos dégradé blanc violet'
            title='dessin original : DinosoftLabs (@Flaticon)'
          />
          <span style={{ display: 'inline-block', 'vertical-align': 'middle' }}>
            Balluchon - Portes et entrées
          </span>
        </Titlebar>
        {zoom && center && <Od4mMap zoom={zoom} center={center} />}
        <NotificationCenter />
      </DefaultTheme>
    );
  }
}

export default withRouter(Home);
