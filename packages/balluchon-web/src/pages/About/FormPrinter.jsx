import React, { useState } from 'react';
import { Form, Button } from 'osm-ui-react';
import generatePdfForm from '../../pdfform.ts';

const FormPrinter = () => {
  const [nbPages, setNbPages] = useState(8);
  const [printNumbers, setPrintNumbers] = useState('true');
  const [pageFormat, setPageFormat] = useState('a5-p');

  const [title, setTitle] = useState('');
  return (
    <Form
      style={{
        border: '1px outset #a8a8fd63',
        padding: '0.5rem',
        margin: '1rem',
        'border-radius': '0.7rem',
        width: 'auto',
      }}
    >
      <Form.Label htmlFor='nbPages'>Nombre de pages : </Form.Label>
      <Form.Input
        id='nbPages'
        type='number'
        style={{ width: '10rem' }}
        placeholder='nombre'
        min={1}
        max={1024}
        onChange={(event) => setNbPages(event.target.value)}
        value={nbPages}
      />
      <Form.Checkbox
        id='printNumbers'
        label='Préremplir les numéros de porte'
        onChange={(event) => setPrintNumbers(event.checked)}
        value='printNumbers'
        checked={printNumbers}
      />
      <Form.Label htmlFor='format'>Format :</Form.Label>
      <Form.Select
        id='pageFormat'
        multi={false}
        clearable={false}
        disabled={true}
        options={[
          { label: 'A5 portrait', value: 'a5-p' },
          { label: 'A4 portrait', value: 'a4-p' },
        ]}
        style={{ width: '10rem' }}
        onChange={(event) => setPageFormat(event.target.value)}
        value={pageFormat}
      />
      <Form.Label htmlFor='title' style={{ display: 'inline' }}>
        {' '}
        Votre titre :
      </Form.Label>
      <Form.Input
        id='title'
        type='text'
        style={{ width: window.innerWidth < 1000 ? '100%' : '24rem' }}
        onChange={(event) => setTitle(event.target.value)}
        placeholder={'Balade du 17-janvier 2021 - quartier Jaurès'}
        value={title}
      />
      <Button
        onClick={() =>
          generatePdfForm({ nbPages, printNumbers, pageFormat, title })
        }
      >
        Générer le pdf
      </Button>
    </Form>
  );
};

export default FormPrinter;

// some more options we could add :

// préremplir l'adresse : rue, ville, code postal

// Préprer pour impression livret
// Marge haut supplémentaire
// MArge gauche supplémentaire
