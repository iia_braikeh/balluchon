import React from 'react';
import { Sidebar, DefaultTheme, BlueTheme, Button, Link } from 'osm-ui-react';
import FormPrinter from './FormPrinter';
import logo from '../../assets/logo/balluchon-150.png';

const About = ({ opened, onClose }) => {
  const openLink = (link) => window.open(link, '_blank');

  return (
    <DefaultTheme>
      <Sidebar
        tinyHeader={window.innerWidth < 800 || window.innerHeight < 500}
        tinyFooter={window.innerWidth < 800 || window.innerHeight < 500}
        opened={opened}
        footer={
          <Button block onClick={onClose}>
            Fermer
          </Button>
        }
        onClickBack={onClose}
        onClickClose={onClose}
        width={window.innerWidth < 1000 ? 'md' : 'lg'}
        stickbottom
        fullheight={true}
        maximized={window.innerWidth < 800}
      >
        <div>
          <img
            src={logo}
            width={'100px'}
            height={'100px'}
            style={{ 'vertical-align': 'bottom', display: 'inline-block' }}
            alt='logo Balluchon : sac-à-dos dégradé blanc violet'
            title='dessin original : DinosoftLabs (@Flaticon)'
          />
          <h3
            style={{
              'vertical-align': 'bottom',
              display: 'inline-block',
              margin: 'auto',
            }}
          >
            Balluchon - Portes et entrées
          </h3>
          <p
            style={{
              'font-size': 'xx-small',
              width: '100px',
              'line-height': '0.7rem',
            }}
          >
            dessin original DinosoftLabs @Flaticon
          </p>
        </div>
        <BlueTheme>
          <p>
            Balluchon vise à faciliter la contribution aux données
            d'accessibilité et de mobilité douce sur la base de données
            géographiques OpenStreetMap. Vous êtes ici sur l'outil de
            contribution pour les{' '}
            <strong>portes et entrées de bâtiments</strong>.
          </p>
          <p></p>
        </BlueTheme>
        <h4>Comment utiliser Balluchon ?</h4>
        <p> Nous vous proposons une vidéo explicative </p>
        <p>
          Afin de préparer vos relevés sur le terrain, voici quelques supports à
          télécharger et imprimer :
        </p>
        <ul>
          <li>
            des{' '}
            <Link
              onClick={() =>
                openLink('https://compas.limos.fr/blog/outils-osm-papier')
              }
              title='https://compas.limos.fr/blog/outils-osm-papier'
            >
              outils pour générer des fonds de carte
            </Link>
          </li>
          <li>
            des "fiches d'identité" à remplir pour chaque porte rencontrée :
            <br />
            <FormPrinter />
          </li>
        </ul>
        <p>
          Si vous souhaitez organiser des ateliers de contribution à l'aide de
          Balluchon,
          {/* voici
          <Link
            href='https://od4m.limos.fr/ateliers' 
            title='https://od4m.limos.fr/ateliers'
          >
            quelques éléments et pistes de scénario
          </Link>
          plus détaillés.
          <br /> N'hésitez pas à */}
          n'hésitez pas à
          <Link
            href='https://compas.limos.fr/contact'
            title='https://compas.limos.fr/contact'
          >
            nous contacter
          </Link>
          pour échanger, nous faire un retour ou partager vos idées à ce sujet.
        </p>
        <h4>A quoi servent ces données ?</h4>
        <p>
          OpenStreetMap est une base de données géographique libre, c'est à dire
          que toutes les données sont légalement réutilisables par tous, et pour
          toutes applications pratiques. Ces applications sont, par exemple :
        </p>
        <ul>
          <li>routage entre plusieurs points ;</li>
          <li>
            description textuelle et sonore de carrefours, de plan de ville ;
          </li>
          <li>impression de cartes tactiles ;</li>
          <li>
            parcours libre avec (audio)description sur plusiseurs niveaux de
            lecture ;
          </li>
        </ul>
        <p>
          Les outils développés par OD4M s'inscrivent dans un projet de
          recherche qui s'intéresse à ces applications : Cartographie et Outils
          Multisensoriels Pour l'Accessibilité Spatiale{' '}
          <span style={{ 'white-space': 'nowrap' }}>
            (
            <Link
              href='https://compas.limos.fr'
              title='https://compas.limos.fr'
            >
              COMPAS
            </Link>
            )
          </span>
          .
        </p>
        <h4>A propos</h4>
        <p>
          Balluchon est l'éditeur pour Open Street Map né du projet Open Data
          for Mobility{' '}
          <span style={{ 'white-space': 'nowrap' }}>
            (
            <Link
              href='https://compas.limos.fr/OD4M'
              title='https://compas.limos.fr/OD4M'
            >
              OD4M
            </Link>
            )
          </span>
          , mené conjointement par le laboratoire du LIMOS et l'entreprise{' '}
          <Link href='https://www.wegoto.eu' title='https://www.wegoto.eu'>
            WEGOTO,
          </Link>
          grâce à un financement de l'Union Nationale des Aveugles et Déficients
          Visuels{' '}
          <span style={{ 'white-space': 'nowrap' }}>
            (
            <Link href='https://www.unadev.com' title='https://www.unadev.com'>
              UNADEV
            </Link>
            )
          </span>
          .
        </p>
        <Button
          onClick={() => openLink('https://od4m.limos.fr')}
          title='https://od4m.limos.fr'
        >
          Plus d'informations ici
        </Button>
        <p>
          Les
          <Link
            href='https://gitlab.limos.fr/iia_braikeh/balluchon'
            title='https://gitlab.limos.fr/iia_braikeh/balluchon'
          >
            sources
          </Link>
          sont disponibles sous licence AGPL-3.
        </p>
        <p>
          Envie de nous raconter votre expérience, de nous aider à améliorer
          Balluchon, besoin d'aide pour le déployer chez vous ?
          <br />
          <Link
            href='https://compas.limos.fr/contact'
            title='https://compas.limos.fr/contact'
          >
            Contactez-nous !
          </Link>
        </p>
        <h4>Données personnelles</h4>
        <p>
          Aucune information ne persiste sur la base de données de Balluchon
          après la fermeture de votre session.
        </p>
      </Sidebar>
    </DefaultTheme>
  );
};

export default About;
