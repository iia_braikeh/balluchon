import L from 'leaflet';

export function pointInPolygon(point, vs) {
  // ray-casting algorithm based on
  // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
  var x = point[0],
    y = point[1];
  var inside = false;
  for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
    var xi = vs[i][0],
      yi = vs[i][1];
    var xj = vs[j][0],
      yj = vs[j][1];
    //prettier-ignore
    var intersect = ((yi > y) !== (yj > y)) && (x < ((xj - xi) * (y - yi)) / (yj - yi) + xi);
    if (intersect) inside = !inside;
  }
  return inside;
}

export function simulateBox(map, center, mapZoom) {
  const pixWidth = 200;
  const pixOffsetX = pixWidth / 2;
  const pixOffsetY = (pixOffsetX * 9) / 16;

  const centerPoint = map.project(center, mapZoom);
  const latLng1 = map.unproject(
    L.point([centerPoint.x - pixOffsetX, centerPoint.y + pixOffsetY]),
    mapZoom
  );
  const latLng2 = map.unproject(
    L.point([centerPoint.x + pixOffsetX, centerPoint.y - pixOffsetY]),
    mapZoom
  );
  const bbox = L.latLngBounds(latLng1, latLng2);
  //check
  // console.log("CHEK", bbox.contains(center));
  return bbox;
}

export function boundsToRectangle(bounds) {
  const { _northEast, _southWest } = bounds;
  return [
    [_southWest.lat, _southWest.lng],
    [_northEast.lat, _northEast.lng],
  ];
}

export function flattenBounds(bounds) {
  const { _northEast, _southWest } = bounds;
  let flatBounds = [
    _southWest.lat,
    _southWest.lng,
    _northEast.lat,
    _northEast.lng,
  ];
  flatBounds = flatBounds.join();
  return flatBounds;
}
