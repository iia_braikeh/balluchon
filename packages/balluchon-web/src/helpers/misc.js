// unused.
export function removed(oldTags, newTags) {
  let removed = [];
  let key;

  for (key in oldTags) {
    if (oldTags.hasOwnProperty(key) && !newTags.hasOwnProperty(key)) {
      removed.push(key);
    }
  }
  return removed.length === 0 ? null : removed;
}
