import L from 'leaflet';
import icon from 'leaflet/dist/images/marker-icon.png';
import iconRetina from 'leaflet/dist/images/marker-icon-2x.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import yellowCircle from '../assets/yellowCircle.png';
import redCircle from '../assets/redCircle.png';
import purpleCircle from '../assets/purpleCircle.png';
delete L.Icon.Default.prototype._getIconUrl;

// L.Icon.Default.mergeOptions({
//     iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
//     iconUrl: require('../assets/marker.png'),
//     shadowUrl: null
// });
// let circleIcon = new L.Icon.Default({});

let yellowCircleIcon = L.icon({
  iconUrl: yellowCircle,
  iconSize: [20, 20],
  // shadowSize: [50, 64],
  iconAnchor: [10, 10],
  // shadowAnchor: [4, 62],
  // popupAnchor: [-3, -76]
});
let redCircleIcon = L.icon({
  iconUrl: redCircle,
  iconSize: [20, 20],
  iconAnchor: [10, 10],
});
let purpleCircleIcon = L.icon({
  iconUrl: purpleCircle,
  iconSize: [20, 20],
  iconAnchor: [10, 10],
});

export { yellowCircleIcon, redCircleIcon, purpleCircleIcon };
