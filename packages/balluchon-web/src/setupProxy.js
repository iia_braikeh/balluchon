// // const proxy = require("http-proxy-middleware");
// const { createProxyMiddleware } = require('http-proxy-middleware');
// module.exports = function (app) {
//     app.use(createProxyMiddleware('/auth', { target: "http://localhost:5000/", secure: false, ws: true, debug: true }));
//     // app.use(createProxyMiddleware('/user', { "target": 'http://localhost:5000/', "secure": false }));
// };

const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function (app) {
  app.use(
    createProxyMiddleware('/auth', {
      target: 'http://localhost:5000',
      prependPath: false,
      logLevel: 'debug',
      secure: false,
    })
  );
  app.use(
    createProxyMiddleware('/api', {
      target: 'http://localhost:5000',
      prependPath: false,
      logLevel: 'debug',
      secure: false,
    })
  );
};

// changeOrigin: true,
