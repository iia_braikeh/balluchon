const buildingQuestions = [
  {
    question: 'Type d’entrée',
    tag: 'entrance',
    type: 'select',
    options: [
      { label: 'entrée', value: 'yes' },
      // { label: 'entrée privée', value: 'home' },
      { label: 'entrée principale', value: 'main' },
      { label: 'entrée secondaire', value: 'secondary' },
      { label: 'entrée de service', value: 'service' },
      { label: 'donne sur une cage d’escalier', value: 'staircase' },
      { label: 'sortie uniquement', value: 'exit' },
      { label: 'sortie de secours', value: 'ermergency' },
    ],
  },
  {
    question: 'Type de porte',
    tag: 'door',
    type: 'select',
    options: [
      { label: 'oui/autre', value: 'yes' },
      { label: 'porte battante simple', value: 'hinged' },
      { label: 'garage, s’ouvre vers le haut', value: 'overhead' },
      { label: 'tambour, tourniquet', value: 'revolving' },
      { label: 'coulissante', value: 'sliding' },
      { label: 'double battants', value: 'double' },
      { label: 'pas de porte', value: 'no' },
    ],
  },
  {
    question: 'Accès',
    tag: 'access',
    type: 'select',
    options: [
      { label: 'autorisé au public', value: 'yes' },
      { label: 'privé', value: 'private' },
      { label: 'réservé aux clients', value: 'customers' },
      { label: 'réservé aux livraisons', value: 'delivery' },
      // { label: 'interdit au public', value: 'no' },
    ],
  },
  {
    question: 'Porte automatique',
    tag: 'automatic_door',
    type: 'select',
    options: [
      { label: 'oui', value: 'yes' },
      { label: 'non', value: 'no' },
    ],
  },
  {
    question: 'Accessible en fauteuil roulant',
    tag: 'wheelchair',
    type: 'select',
    options: [
      { label: 'oui', value: 'yes' },
      { label: 'non', value: 'no' },
      { label: 'limité, avec rampe amovible', value: 'limited' },
    ],
  },
  {
    question: 'Largeur du passage (en mètres)',
    tag: 'width',
    type: 'number',
  },
  {
    question: 'Niveau (étage)',
    tag: 'level',
    type: 'number',
  },
  // entrance-steps (number) ?
  // entrance-ramp (number) ?
  // s'ouvre vers l'intérieur / extérieur ?
  // digicode / etc
  // address (remplir si pas déjà sur le bâtiment ! -> vidéos formation)
];

const addressQuestions = [
  {
    question: 'Numéro de voie',
    tag: 'addr:housenumber',
    placeholder: '1, 2, 2bis',
    type: 'text',
    style: { width: '50%' },
  },
  {
    question: 'Voie',
    tag: 'addr:street',
    placeholder: 'Place de la Fontaine, Rue Louis Braille',
    type: 'select',
    moreOptions: 'streets',
  },
  {
    question: 'Code postal',
    tag: 'addr:postcode',
    placeholder: '',
    type: 'number',
  },
  {
    question: 'Ville/commune',
    tag: 'addr:city',
    placeholder: '',
    type: 'select',
    moreOptions: 'cities',
  },
];

const questions = {
  doorOfBuildingTags: buildingQuestions,
  doorAddressTags: addressQuestions,
};
export default questions;
