// notifications
const notificate = {
  zoom17atleast: { id: 17, content: 'Un zoom supérieur à 17 est nécessaire' },
  notConnected: { id: 1, content: 'Connectez-vous pour éditer' },
  fetchingError: { id: 2, content: 'La requête a échoué, essayez de nouveau' },

  // new door
  fixDoorMarker: { id: 11, content: 'Fixez le point avant de continuer' },
  doorTagsTooLight: {
    id: 12,
    content: "Précisez le type d'entrée ou de porte",
  },
};
export default notificate;
