require('dotenv').config();
const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const mongoose = require('mongoose');
const morgan = require('morgan');
const path = require('path');
// const fs = require('fs');
// const http = require('http');
// const https = require('https');
global.XMLHttpRequest = require('node-http-xhr');

const passport = require('./passport/setup');
const authRoutes = require('./routes/auth.js');
const apiRoutes = require('./routes/api.js');

const app = express();
app.use(helmet());
app.use(morgan('dev'));

const corsOptions = { credentials: true };
app.use(cors(corsOptions));

const MONGO_URI = process.env.MONGO_URI;
mongoose
  .connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(console.log(`MongoDB connected ${MONGO_URI}`))
  .catch((err) => console.log(err));

// Bodyparser middleware, extended false does not allow nested payloads
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// // Express Session
app.use(
  session({
    secret: process.env.SESSION_SECRET || 'keyboard cat',
    resave: false,
    saveUninitialized: false, // afficher qu'on stocke un cookie
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    cookie: {
      // secure: process.env.NODE_ENV === "production",
      httpOnly: process.env.NODE_ENV === 'production',
    },
    rolling: true, //  to check
  })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// serve the react app files
if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, '../html/build')));
  app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '../html/build', 'index.html'));
  });
} else {
  app.use(express.static(`../react-pass2/build`));
}

// Routes
app.use('/auth', authRoutes);
app.use('/api', apiRoutes);

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Backend listening on port ${PORT}!`));
