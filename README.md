# Balluchon

<img src="./logo/balluchon.png"  width="120" height="120">
*original work designed by DinosoftLabs from Flaticon*

Balluchon is a simple tool to contribute to OpenStreetMap, focusing on pedestrian-friendly data, for non-experts.
At this point, it helps to add doors/entrances or modify available doors/entrances along buildings.

## Demo

The LIMOS provides an instance here : https://od4m.limos.fr/balluchon

## Context
OpenStreetMap data is 
Pedestrian oriented data

There are some good tools that already aim at

StreetComplete
(check the [OSM wiki](https://wiki.openstreetmap.org/wiki/StreetComplete) or [StreetComplete website](https://github.com/streetcomplete/StreetComplete))

MapContrib :

With Balluchon, we want to propose a tool for non-experts to add geometry without the worry of making incidental unintentional changes.

supports use cases

## Features

What's all the bells and whistles this project can perform?

    What's the main functionality
    You can also do another thing
    If you get really randy, you can even do this


### Proposed features


## Getting started

### 1. Get Balluchon and install dependencies
First clone the repository 
```
git clone https://gitlab.limos.fr/iia_braikeh/balluchon.git
```

Install lerna cli tool globally :
```
npm install -g lerna
```

Install Balluchon's dependencies 
```
cd balluchon/
lerna bootstrap
```

### 2. Provide a MongoDB database.
Balluchon uses MongoDB to handle sessions data.
You can for example use a Docker container to handle it.
```bash
docker pull mongo
docker run -it -v /data/db:/balluchon-mongodata -p 27017:27017 --name balluchon-mongodb -d mongo
```

### 3. Initial Configuration
Copy `.env.sample` file to `.env`. This is the config file.

Register your app in OpenStreetMap with your account to get an access token.
For testing purposes, you should use the development instance of OpenStreetMap.
If using the development instance of OpenStreetMap, you have to modify

You need to put some session secret key.
Mapbox token 
Mongo DB address

ENV=dev

Also ensure `balluchon-server/node_modules/osm-request/dist/OsmRequest.js` has been correctly patched with these two line at the top of the file :
```
let xmldoc = require('xmldoc');
let XMLDocument = xmldoc.XmlDocument;
```

#### 4.Launch the server and the client together
```
npm run start
```
or launch the server and the client separately with `npm run start-server` and `npm run start-web`

Navigate on `localhost:3000` !
You have now access to Balluchon.

## Developing

Here's a brief intro about what a developer must do in order to start developing the project further:

First clone the repository 
```
git clone https://gitlab.limos.fr/iia_braikeh/balluchon.git
```
Then install dependencies 
```
npm install -g lerna
cd balluchon/
lerna bootstrap
```
~~or with `npm install` from `packages/balluchon-server` and `packages/balluchon-web` directories~~

And state what happens step-by-step.

### (Continuous Integration)
.. Osm ui react ..

## Deploying / Publishing

In case there's some step you have to take that publishes this project to a server, this is the right time to state it.

For production, you need to build the React app (the client).
```
npm run build-web
```
or
```
cd packages/balluchon-web && npm run build
```

And again you'd need to tell what the previous code actually does.

You may want to have a look at :
- nginx configuration example
- deamon script example for the server

## Contributing

When you publish something open source, one of the greatest motivations is that anyone can just jump in and start contributing to your project.

These paragraphs are meant to welcome those kind souls to feel that they are needed. You should state something like:

"If you'd like to contribute, please fork the repository and use a feature branch. Pull requests are warmly welcome."

### Translation



### Development

If there's anything else the developer needs to know (e.g. the code style guide), you should link it here. If there's a lot of things to take into consideration, it is common to separate this section to its own file called CONTRIBUTING.md (or similar). If so, you should say that it exists here.

### Proposed features
Proposed features are posted in the issue tracker

### User feedback
...

### Testing and reporting issues
...


## Links

### Balluchon project

Project homepage: https://od4m.limos.fr
Repository: https://gitlab.limos.fr/balluchon/
Issue tracker: https://gitlab.limos.fr/balluchon/issues

*In case of sensitive bugs like security vulnerabilities, please contact my@email.com directly instead of using issue tracker. We value your effort to improve the security and privacy of this project!*

### Balluchon related projects

#### Dependencies
OSM-UI-React (on top of Leaflet-React)
OSM-Request

#### Related projects
MapContrib [http://www.mapcontrib.xyz](http://www.mapcontrib.xyz)
MapContrib next: [https://gitlab.com/mapcontrib/mapcontrib.next](https://gitlab.com/mapcontrib/mapcontrib.next)
StreetComplete : (https://github.com/streetcomplete/StreetComplete](https://github.com/streetcomplete/StreetComplete)

## License

The code in this project is licensed under AGPL-3 license.

## Sponsors
Unadev

